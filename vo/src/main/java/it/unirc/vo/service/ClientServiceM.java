package it.unirc.vo.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.unirc.vo.dto.*;
import it.unirc.vo.dto.device.Attributes;
import it.unirc.vo.dto.device.DeviceResponse;
import it.unirc.vo.dto.device.ObjectLinksItem;
import it.unirc.vo.dto.observe.*;
import it.unirc.vo.model.Object;
import it.unirc.vo.model.Observer;
import it.unirc.vo.model.Resource;
import it.unirc.vo.model.ValueInflux;
import it.unirc.vo.mqtt.MqClient;
import it.unirc.vo.repository.*;
import it.unirc.vo.selection.MQTTCondition;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
@Conditional(MQTTCondition.class)
@Slf4j
public class ClientServiceM implements ClientService {
    private final DeviceRepository deviceRepository;
    private final ObjectRepository objectRepository;
    private final ResourceRepository resourceRepository;
    private final ObserverRepository observerRepository;
    private final MqClient mqClient;
    private final ObjectMapper objectMapper;
    private final boolean composite;

    private final InfluxFunctions influxFunctions;

    @Autowired
    public ClientServiceM(DeviceRepository deviceRepository, ObjectRepository objectRepository,
            ResourceRepository resourceRepository, ObserverRepository observerRepository,
            MqClient mqClient, ObjectMapper objectMapper, boolean composite,
            InfluxFunctions influxFunctions) {
        this.deviceRepository = deviceRepository;
        this.objectRepository = objectRepository;
        this.resourceRepository = resourceRepository;
        this.observerRepository = observerRepository;

        this.mqClient = mqClient;
        this.objectMapper = objectMapper;
        this.composite = composite;
        this.influxFunctions = influxFunctions;
    }

    public DeviceResponse getDeviceResponse(String endpoint) {
        DeviceResponse response = new DeviceResponse();
        deviceRepository.findByEndpoint(endpoint).ifPresent(e -> {
            response.setEndpoint(e.getEndpoint());
            response.setAddress(e.getAddress());
            response.setBindingMode(e.getBindingMode());
            List<ObjectLinksItem> objectLinks = new ArrayList<>();
            // objectLinks.add(new ObjectLinksItem("/", new
            // Attributes(e.getResourceType())));
            e.getObjectLinks().forEach(o -> {
                objectLinks.add(new ObjectLinksItem("/" + o.getObjectId() + "/" + o.getInstanceId(),
                        new Attributes(e.getResourceType())));
            });
            response.setObjectLinks(objectLinks);
            response.setVersion(e.getLwM2mVersion());
            response.setLastUpdate(e.getUpdatedAt().toString());
            response.setRegistrationId(e.getRegistrationId());
            response.setRegistrationDate(e.getCreatedAt().toString());
            response.setLifetime(e.getLifetime());
            response.setRootPath(e.getRootPath());
            response.setSecure(false);
        });
        return response;
    }

    public ObjectResponseDto getObjectResponse(String endpoint, String objectId) {
        ObjectResponseDto response = new ObjectResponseDto();
        response.setStatus("CONTENT");
        ContentObjectDto content = new ContentObjectDto();
        content.setId(objectId);
        List<ObjectInstanceDto> objectInstanceDtos = new ArrayList<>();
        objectRepository.findByEndpointObjectId(endpoint, objectId).forEach(object -> {
            ObjectInstanceDto objectInstanceDto = new ObjectInstanceDto();
            objectInstanceDto.setId(object.getInstanceId());
            List<ResourceDto> resourceDtos = new ArrayList<>();
            object.getResources().forEach(resource -> {
                ResourceDto resourceDto = new ResourceDto();
                resourceDto.setId(resource.getResourceId());
                if (resource.getValues().size() != 0) {
                    resourceDto.setValue(resource.getValues().get(0).getValue());
                    resourceDto.setTimestamp(resource.getValues().get(0).getCreatedAt().toString());
                }
                resourceDtos.add(resourceDto);
            });
            objectInstanceDto.setResources(resourceDtos);
            objectInstanceDtos.add(objectInstanceDto);
        });
        content.setInstances(objectInstanceDtos);
        response.setContent(content);
        return response;
    }

    public ObjectInstanceResponseDto getObjectInstanceResponse(String endpoint, String objectId, String instanceId,
            HttpServletRequest request) {
        ObjectInstanceResponseDto response = new ObjectInstanceResponseDto();
        response.setStatus("CONTENT");
        ContentResourceDto content = new ContentResourceDto();
        content.setId(objectId + "/" + instanceId);
        List<ResourceDto> resourceDtos = new ArrayList<>();
        // objectRepository.findByObjectIdAndInstanceId(objectId,
        // instanceId).ifPresent(object -> {
        objectRepository.findByEndpointObjectIdInstanceId(endpoint, objectId, instanceId).ifPresent(object -> {
            object.getResources().forEach(resource -> {
                ResourceDto resourceDto = new ResourceDto();
                resourceDto.setId(resource.getResourceId());
                if (resource.getValues().size() != 0) {
                    resourceDto.setValue(resource.getValues().get(0).getValue());
                    resourceDto.setTimestamp(resource.getValues().get(0).getCreatedAt().toString());
                } else {
                    this.addObserverToObjectInstanceOneShot(request, object);
                }
                resourceDtos.add(resourceDto);
            });
        });
        content.setResources(resourceDtos);
        response.setContent(content);
        return response;
    }

    public ResourceResponseDto getResourceResponse(String endpoint, String objectId, String instanceId,
            String resourceId, HttpServletRequest request, boolean isRealtime) {
        ResourceResponseDto response = new ResourceResponseDto();
        response.setStatus("CONTENT");
        ResourceDto resourceDto = new ResourceDto();
        resourceDto.setId(resourceId);
        objectRepository.findByEndpointObjectIdInstanceId(endpoint, objectId, instanceId).ifPresent(object -> {
            object.getResources().stream().filter(r -> r.getResourceId().equals(resourceId))
                    .findFirst()
                    .ifPresent(resource -> {
                        /*
                         * if (resource.getValues().size() != 0) {
                         * resourceDto.setValue(resource.getValues().get(0).getValue());
                         * resourceDto.setTimestamp(resource.getValues().get(0).getCreatedAt().toString(
                         * ));
                         * } else if (resource.getValues().size() == 0 || isRealtime) {
                         * this.addObserverToResourceOneShot(request, resource);
                         * }
                         *
                         */

                        List<ValueInflux> listOfValue = influxFunctions
                                .queryAllDataMod(resource.getId().toString() /*, resource.getType()*/);
                        if (listOfValue.size() != 0) {
                            ValueInflux firstValue = listOfValue.get(0);
                            resourceDto.setValue(firstValue.getValue().toString());
                            log.info("FIRST value.getValue.tostring()" + firstValue.getValue().toString());
                            resourceDto.setTimestamp(firstValue.getTime().toString());
                        } else if (listOfValue.size() == 0 || isRealtime) {
                            this.addObserverToResourceOneShot(request, resource);
                        }

                    });
        });
        response.setContent(resourceDto);
        return response;
    }

    public ObserveObjectResponse observeObjectInstance(String endpoint, String objectId, String instanceId,
            String obsAddress,
            HttpServletRequest request) {
        String observerAddr;
        if (obsAddress == null) {
            observerAddr = request.getRemoteAddr();
        } else {
            observerAddr = checkURL(obsAddress);
        }
        log.info("New Observer Address: " + observerAddr + " for endpoint: " + endpoint + "/" + objectId
                + "/" + instanceId);
        if (this.composite) {
            ObserveObjectResponse response = new ObserveObjectResponse();
            deviceRepository.findByEndpoint(endpoint).ifPresent(device -> {
                objectRepository.findByEndpointObjectIdInstanceId(endpoint, objectId, instanceId).ifPresent(object -> {
                    if (!observerRepository.existsByObservable_Id(object.getId())) {
                        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl("http://" + device.getAddress())
                                .addConverterFactory(JacksonConverterFactory.create())
                                .client(httpClient.build())
                                .build();

                        CompositeService service = retrofit.create(CompositeService.class);
                        Call<ObserveObjectResponse> callSync = service.observeObjectInstance(endpoint, objectId,
                                instanceId);
                        try {
                            Response<ObserveObjectResponse> r = callSync.execute();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                });

                response.setEvent("NOTIFICATION");
                ObserveObjectData data = new ObserveObjectData();
                data.setEp(endpoint);
                data.setRes("/" + objectId + "/" + instanceId);
                ObserveObjectVal val = new ObserveObjectVal();
                val.setId(instanceId);
                List<ResourceDto> resources = new ArrayList<>();
                objectRepository.findByEndpointObjectIdInstanceId(endpoint, objectId, instanceId).ifPresent(object -> {
                    this.addObserverToObjectInstance(observerAddr, object);
                    object.getResources().forEach(r -> {
                        ResourceDto resourceDto = new ResourceDto();
                        resourceDto.setId(r.getResourceId());
                        if (r.getValues().size() != 0) {
                            resourceDto.setValue(r.getValues().get(0).getValue());
                            resourceDto.setTimestamp(r.getCreatedAt().toString());
                        }
                        resources.add(resourceDto);
                    });
                });
                val.setResources(resources);
                data.setVal(val);
                response.setData(data);
            });
            return response;
        } else {
            ObserveObjectResponse response = new ObserveObjectResponse();
            response.setEvent("NOTIFICATION");
            ObserveObjectData data = new ObserveObjectData();
            data.setEp(endpoint);
            data.setRes("/" + objectId + "/" + instanceId);
            ObserveObjectVal val = new ObserveObjectVal();
            val.setId(instanceId);
            List<ResourceDto> resources = new ArrayList<>();
            objectRepository.findByObjectIdAndInstanceId(objectId, instanceId).ifPresent(object -> {
                this.addObserverToObjectInstance(observerAddr, object);
                object.getResources().forEach(r -> {
                    ResourceDto resourceDto = new ResourceDto();
                    resourceDto.setId(r.getResourceId());
                    if (r.getValues().size() != 0) {
                        resourceDto.setValue(r.getValues().get(0).getValue());
                        resourceDto.setTimestamp(r.getCreatedAt().toString());
                    }
                    resources.add(resourceDto);
                });
            });
            val.setResources(resources);
            data.setVal(val);
            response.setData(data);
            return response;
        }
    }

    public String checkURL(String address) {
        String result = address;
        if (!address.startsWith("http")) {
            result = "http://" + address;
        }
        if (!address.contains(":")) {
            result = result + ":8080";
        }
        return result;
    }

    public ObserveResourceResponse observerResource(String endpoint, String objectId, String instanceId,
            String resourceId, String obsAddress, HttpServletRequest request) {
        String observerAddr;
        if (obsAddress == null) {
            observerAddr = request.getRemoteAddr();
        } else {
            observerAddr = checkURL(obsAddress);

        }

        log.info("New Observer Address: " + observerAddr + " for endpoint: " + endpoint + "/" + objectId
                + "/" + instanceId + "/" + resourceId);
        if (this.composite) {
            ObserveResourceResponse response = new ObserveResourceResponse();
            deviceRepository.findByEndpoint(endpoint).ifPresent(device -> {
                resourceRepository
                        .findResourceByDeviceObjectInstanceResourceId(endpoint, objectId, instanceId, resourceId)
                        .ifPresent(resource -> {
                            if (!observerRepository.existsByObservable_Id(resource.getId())) {
                                OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
                                Retrofit retrofit = new Retrofit.Builder()
                                        .baseUrl("http://" + device.getAddress())
                                        .addConverterFactory(JacksonConverterFactory.create())
                                        .client(httpClient.build())
                                        .build();

                                CompositeService service = retrofit.create(CompositeService.class);
                                Call<ObserveResourceResponse> callSync = service.observeResource(endpoint, objectId,
                                        instanceId, resourceId);
                                try {
                                    Response<ObserveResourceResponse> r = callSync.execute();
                                    ObserveResourceResponse observeResourceResponse = r.body();
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        });

                response.setEvent("NOTIFICATION");
                ObserveResourceData data = new ObserveResourceData();
                data.setEp(endpoint);
                data.setRes("/" + objectId + "/" + instanceId + "/" + resourceId);
                ResourceDto resourceDto = new ResourceDto();
                resourceDto.setId(resourceId);
                objectRepository.findByEndpointObjectIdInstanceId(endpoint, objectId, instanceId)
                        .ifPresent(object -> {
                            object.getResources().stream().filter(r -> r.getResourceId().equals(resourceId))
                                    .findFirst()
                                    .ifPresent(resource -> {
                                        if (resource.getValues().size() != 0) {
                                            resourceDto.setValue(resource.getValues().get(0).getValue());
                                            resourceDto.setTimestamp(resource.getCreatedAt().toString());
                                        }
                                        this.addObserverToResource(observerAddr, resource);
                                    });
                        });
                data.setVal(resourceDto);
                response.setData(data);
            });
            return response;
        } else {
            ObserveResourceResponse response = new ObserveResourceResponse();
            response.setEvent("NOTIFICATION");
            ObserveResourceData data = new ObserveResourceData();
            data.setEp(endpoint);
            data.setRes("/" + objectId + "/" + instanceId + "/" + resourceId);
            ResourceDto resourceDto = new ResourceDto();
            resourceDto.setId(resourceId);
            objectRepository.findByObjectIdAndInstanceId(objectId, instanceId).ifPresent(object -> {
                object.getResources().stream().filter(r -> r.getResourceId().equals(resourceId))
                        .findFirst()
                        .ifPresent(resource -> {
                            if (resource.getValues().size() != 0) {
                                resourceDto.setValue(resource.getValues().get(0).getValue());
                                resourceDto.setTimestamp(resource.getCreatedAt().toString());
                            }
                            this.addObserverToResource(observerAddr, resource);
                        });
            });
            data.setVal(resourceDto);
            response.setData(data);
            return response;
        }
    }

    public void deleteObserveObjectInstance(String endpoint, String objectId, String instanceId, String obsAddress,
            HttpServletRequest request) {
        String observerAddr;
        if (obsAddress == null) {
            observerAddr = request.getRemoteAddr();
        } else {
            observerAddr = checkURL(obsAddress);
        }
        log.info("Deleting Observer Address: " + observerAddr + "for Instance: " + endpoint + "/" + objectId + "/"
                + instanceId);
        objectRepository
                .findByEndpointObjectIdInstanceId(endpoint, objectId, instanceId)
                .ifPresent(object -> {
                    object.getObservers().stream()
                            .filter(observer -> observer.getAddress().equals(observerAddr)).findFirst()
                            .ifPresent(observer -> {
                                object.getObservers().remove(observer);
                                observerRepository.delete(observer);
                            });
                });
    }

    public void deleteObserverResource(String endpoint, String objectId, String instanceId, String resourceId,
            String obsAddress,
            HttpServletRequest request) {
        String observerAddr;
        if (obsAddress == null) {
            observerAddr = request.getRemoteAddr();
        } else {
            observerAddr = checkURL(obsAddress);
        }
        log.info("Deleting Observer Address: " + observerAddr + "For Resource:" + endpoint + "/" + objectId + "/"
                + instanceId + "/" + resourceId);
        resourceRepository
                .findResourceByDeviceObjectInstanceResourceId(endpoint, objectId, instanceId, resourceId)
                .ifPresent(resource -> {
                    resource.getObservers().stream()
                            .filter(observer -> observer.getAddress().equals(observerAddr)).findFirst()
                            .ifPresent(observer -> {
                                resource.getObservers().remove(observer);
                                observerRepository.delete(observer);
                            });
                });
    }

    // void addObserverToResource(HttpServletRequest request, Resource resource) {
    public void addObserverToResource(String obsAddress, Resource resource) {

        if (observerRepository.existsByAddressAndObservable_Id(obsAddress, resource.getId()))
            return;
        Observer observer = new Observer();
        observer.setAddress(obsAddress);
        observer.setObservable(resource);
        observerRepository.save(observer);
    }

    private void addObserverToObjectInstanceOneShot(HttpServletRequest request, Object object) {
        if (observerRepository.existsByAddressAndObservable_Id(request.getRemoteAddr(), object.getId()))
            return;
        Observer observer = new Observer();
        observer.setAddress(request.getRemoteAddr());
        observer.setOneShot(true);
        observer.setObservable(object);
        observerRepository.save(observer);
    }

    private void addObserverToResourceOneShot(HttpServletRequest request, Resource resource) {
        if (observerRepository.existsByAddressAndObservable_Id(request.getRemoteAddr(), resource.getId()))
            return;
        Observer observer = new Observer();
        observer.setAddress(request.getRemoteAddr());
        observer.setOneShot(true);
        observer.setObservable(resource);
        observerRepository.save(observer);
    }

    private void addObserverToObjectInstance(String obsAddress, Object object) {
        if (observerRepository.existsByAddressAndObservable_Id(obsAddress, object.getId()))
            return;
        Observer observer = new Observer();
        observer.setAddress(obsAddress);
        observer.setObservable(object);
        observerRepository.save(observer);
    }

    public void writeObjectInstance(String endpoint, String objectId, String instanceId,
            ObjectInstanceDto objectInstanceDto) throws Throwable {
        if (this.composite) {
            deviceRepository.findByEndpoint(endpoint).ifPresent(device -> {
                OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("http://" + device.getAddress())
                        .addConverterFactory(JacksonConverterFactory.create())
                        .client(httpClient.build())
                        .build();

                CompositeService service = retrofit.create(CompositeService.class);
                Call<ResponseBody> callSync = service.putObjectInstance(endpoint, objectId, instanceId,
                        objectInstanceDto);
                try {
                    callSync.execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } else {
            MqttPublishObject object = new MqttPublishObject();
            List<MqttEventDto> mqttEventDtos = new ArrayList<>();
            objectInstanceDto.getResources().forEach(resourceDto -> {
                MqttEventDto mqttEventDto = new MqttEventDto();
                mqttEventDto.setN(resourceDto.getId());
                mqttEventDto.setV(resourceDto.getValue());
                mqttEventDtos.add(mqttEventDto);
            });
            object.setE(mqttEventDtos);
            mqClient.publish("cmnd/" + endpoint + "/" + objectId + "/" + instanceId, 0,
                    objectMapper.writeValueAsBytes(object), false);
        }
    }

    public void writeResource(String endpoint, String objectId, String instanceId, String resourceId,
            ResourceDto resourceDto) throws Throwable {
        if (this.composite) {
            deviceRepository.findByEndpoint(endpoint).ifPresent(device -> {
                OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("http://" + device.getAddress())
                        .addConverterFactory(JacksonConverterFactory.create())
                        .client(httpClient.build())
                        .build();

                CompositeService service = retrofit.create(CompositeService.class);
                Call<ResponseBody> callSync = service.putResource(endpoint, objectId, instanceId, resourceId,
                        resourceDto);
                try {
                    callSync.execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                resourceRepository
                        .findResourceByDeviceObjectInstanceResourceId(endpoint, objectId, instanceId, resourceId)
                        .ifPresent(resource -> {

                            // CONVERTING TO VALUE INFLUX
                            //ValueInflux valueInflux = new ValueInflux();
                            //valueInflux.setResourceId(resource.id);
                            //valueInflux.setValue(resourceDto.getValue());
                            //influxFunctions.writeValue(valueInflux);


                            influxFunctions.writePointValue(resource.id, resourceDto.getValue(), resource.type);

                            /*
                             * Value value = new Value();
                             * value.setResource(resource);
                             * value.setValue(resourceDto.getValue());
                             * valueRepository.save(value);
                             */
                        });
            });
        } else {
            MqttPublishResource resource = new MqttPublishResource(resourceDto.getValue());
            mqClient.publish("cmnd/" + endpoint + "/" + objectId + "/" + instanceId + "/" + resourceId, 0,
                    objectMapper.writeValueAsBytes(resource), false);
        }
    }

    public void executeCommand(String endpoint, String objectId, String instanceId, String resourceId,
            HttpServletRequest request) throws Throwable {
        if (this.composite) {
            resourceRepository
                    .findResourceByDeviceObjectInstanceResourceId(endpoint, objectId, instanceId, resourceId)
                    .ifPresent(resource -> {
                        deviceRepository.findByEndpoint(endpoint).ifPresent(device -> {
                            this.addObserverToResourceOneShot(request, resource);
                            try {
                                OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
                                Retrofit retrofit = new Retrofit.Builder()
                                        .baseUrl("http://" + device.getAddress())
                                        .addConverterFactory(JacksonConverterFactory.create())
                                        .client(httpClient.build())
                                        .build();

                                CompositeService service = retrofit.create(CompositeService.class);
                                Call<ResponseBody> callSync = service.postCommand(endpoint, objectId, instanceId,
                                        resourceId);
                                callSync.execute();
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        });
                    });
        } else {
            resourceRepository
                    .findResourceByDeviceObjectInstanceResourceId(endpoint, objectId, instanceId, resourceId)
                    .ifPresent(resource -> {
                        deviceRepository.findByEndpoint(endpoint).ifPresent(device -> {
                            this.addObserverToResourceOneShot(request, resource);
                            try {
                                mqClient.publish(
                                        "cmnd/" + endpoint + "/" + objectId + "/" + instanceId + "/" + resourceId, 0,
                                        "EXECUTE".getBytes(), false);
                            } catch (Throwable throwable) {
                                throwable.printStackTrace();
                            }

                        });
                    });
        }
    }
}
