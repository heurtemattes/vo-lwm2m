package it.unirc.vo.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.unirc.vo.coap.JSONConv;
import it.unirc.vo.dto.MqttEventDto;
import it.unirc.vo.dto.MqttPayloadDto;
import it.unirc.vo.dto.ResourceDto;
import it.unirc.vo.dto.observe.ObserveObjectData;
import it.unirc.vo.dto.observe.ObserveObjectResponse;
import it.unirc.vo.dto.observe.ObserveObjectVal;
import it.unirc.vo.dto.observe.ObserveResourceData;
import it.unirc.vo.dto.observe.ObserveResourceResponse;
import it.unirc.vo.model.Event;
import it.unirc.vo.model.Resource;
import it.unirc.vo.model.Value;
import it.unirc.vo.model.ValueInflux;
import it.unirc.vo.repository.*;
import lombok.extern.slf4j.Slf4j;

import org.eclipse.leshan.core.response.ObserveResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

@Slf4j
@Service
public class EventService {
    private static final String EVENT_DEREGISTRATION = "DEREGISTRATION";
    private static final String EVENT_UPDATED = "UPDATED";
    private static final String EVENT_REGISTRATION = "REGISTRATION";
    private static final String EVENT_NOTIFICATION = "NOTIFICATION";
    private static final String EVENT_COAP_LOG = "COAPLOG";
    private static final String QUERY_PARAM_ENDPOINT = "ep";
    private static final String EVENT_MQTT_LOG = "MQTTLOG";

    private final EventRepository eventRepository;
    private final ResourceRepository resourceRepository;
    private final DeviceRepository deviceRepository;
    private final ObjectRepository objectRepository;
    private final ValueRepository valueRepository;

    private final ObserverService observerService;

    private final ObjectMapper objectMapper;

    private final NotifyService notifyService;

    InfluxFunctions influxFunctions;

    @Autowired
    public EventService(DeviceRepository deviceRepository,
            EventRepository eventRepository,
            ObjectRepository objectRepository,
            ResourceRepository resourceRepository,
            ValueRepository valueRepository,
            ObserverService observerService,
            ObjectMapper objectMapper,
            NotifyService notifyService,
            InfluxFunctions influxFunctions) {
        this.deviceRepository = deviceRepository;
        this.eventRepository = eventRepository;
        this.objectRepository = objectRepository;
        this.resourceRepository = resourceRepository;
        this.valueRepository = valueRepository;
        this.observerService = observerService;
        this.objectMapper = objectMapper;
        this.notifyService = notifyService;
        this.influxFunctions = influxFunctions;
    }

    public String checkMessageType(Event event) {
        switch (event.getType()) {
            case EVENT_REGISTRATION:
                log.info("\nParseMessage REGISTRATION :");// +jobject.get("data").toString());
                // parseMessage = EventHandler.register(jobject.get("data").toString());
                // EventHandler.fwdEvent(jobject.toString());
                registration(event);
                break;
            case EVENT_UPDATED:
                log.info("\nParseMessage UPDATED :");// +jobject.get("data").toString());
                // parseMessage = EventHandler.update(jobject.get("data").toString());
                // EventHandler.fwdEvent(jobject.toString());
                break;
            case EVENT_DEREGISTRATION:
                log.info("\nParseMessage DEREGISTRATION :");// +jobject.get("data").toString());
                // parseMessage = EventHandler.deregistration(jobject.get("data").toString());
                break;
            case EVENT_NOTIFICATION:
                log.info("\nParseMessage NOTIFICATION :");// +jobject.get("data").toString());
                // parseMessage = EventHandler.notification(jobject.get("data").toString());
                break;
            case EVENT_COAP_LOG:
                log.info("\nCOAPLOG received");// ,jobject.get("data").toString());;
                break;
            case QUERY_PARAM_ENDPOINT:
                log.info("\nQUERY_PARAM_ENDPOINT received");// ,jobject.get("data").toString());
                break;
            case EVENT_MQTT_LOG:
                log.info("\nCOAPLOG received");// ,jobject.get("data").toString());;
                break;

        }
        return null;

    }

    // Use: when at physical device's first connection
    public void registration(Event event) {
        // TODO: Actually not foreseen in MQTT device

        // Forward event in order to manage observation and registration to consumers or
        // Cloud
        // EventHandler.fwdEvent(jsonevent);

    }

    // Use: when the physical device change objects' configuration
    public void updated() {
        // TODO: Actually not foreseen in MQTT device

        // Forward event in order to manage observation and registration to consumers or
        // Cloud
        // EventHandler.fwdEvent(jsonevent);

    }

    public void deregistration() {

    }

    /*
     * MQTT Interface:
     * path is the resource path like: /deviceID/objId/instId/resId
     * payload: is a JSONObject passed as String;
     * i.e. payload:
     * -Resource: {"tmstp":"2020-04-06T10:55:34+02:00","e":[{“v”:"value"}]}
     * -Instance:
     * {"tmstp":"2020-04-06T10:55:34+02:00","e":[{“n”:”resID”,“v”:"value"},{“n”:”
     * resID”,“v”:"value"},{“n”:” resID”,“v”:"value"}]}
     * -object:
     * {"tmstp":"2020-04-06T10:55:34+02:00","e":[{“n”:”instId/resId”,“v”:"value"},{“
     * n”:” instId/resId”,“v”:"value"},{“n”:” instId/resId”,“v”:"value"},{…}]}
     *
     * Leshan-HTTP-NOTIFICATION :
     * Resource:
     * {"event": "NOTIFICATION",
     * "data": {
     * "ep": "giacomo-t6010",
     * "res": "/3303/0/5700",
     * "val": {
     * "id": 5700,
     * "value": 20.9
     * }}}
     * Instance:
     * {"event":"NOTIFICATION",
     * "data":{
     * "ep":"GG-Notebook",
     * "res":"/3303/0",
     * "val":
     * {"id":0,
     * "resources":[{"id":5601,"value":14.1},{"id":5602,"value":22.8},{"id":5700,
     * "value":14.4},{"id":5701,"value":"cel"}]}}}
     */

    public boolean goodPath (Path path){
        if (path.getNameCount() < 3){
            return false;
        }
        return true;
    }

    public String notification(Path path, String pyld) throws JsonProcessingException {
        // This is only for Mqtt packets
        // first we split timestamp (tmsp) and payload

        if (!goodPath(path)){
            return "Error: Path is not corrected";
        }

        MqttPayloadDto payload = objectMapper.readValue(pyld, MqttPayloadDto.class);
        Event eventModel = new Event();
        eventModel.setType("stat");
        eventModel.setSource(objectMapper.writeValueAsString(payload));
        log.info("EventModel.getsource: " + eventModel.getSource());
        // eventRepository.save(eventModel);
        influxFunctions.writeSinglePoint(eventModel, payload, path); // Stores JSON
                                                                                                          // string
        log.info("Event model: " + eventModel.getSource());
        String deviceID = path.subpath(0, 1).toString();
        if (deviceRepository.findByEndpoint(deviceID).isPresent()) {
            // if object/instance -> path = /deviceID/objId/instId
            if (path.getNameCount() < 4) { //D001/3303/0/
                // valueRepository.saveAll(this.notifyObjInst(payload, saveALL,
                // payload.getTmstp())); //BOTTLENECK
                //
                // inConn.writeMultiplePoints(influxDBClient, payload, path); //Stores atomic
                // resource values

                //influxFunctions.writeMultiplePoints(payload, path);

                notifyObjInst(payload, path);

                return "success";
            }
            // if resource -> path = /deviceID/objId/instId/resource
            else {


                notifyRes(payload.getE().get(0), path);
                return "success";
            }
        } else {
            return "error: device not found";
        }
    }

    /*
     * Arriva il payload {“v”:"value"} OPPURE da notifyObjInstance {“n”:”
     * resID”,“v”:"value"}
     * invio agli observer il notify e salvo ogni singolo value nell'entity VALUE
     */
    private void notifyRes(MqttEventDto event, Path path) {
        String deviceID = path.subpath(0, 1).toString();
        String objId = path.subpath(1, 2).toString();
        String instId = path.subpath(2, 3).toString();

        // Al posto di value noi salviamo in influx come ValueInflux
        // Value value = new Value();

        ValueInflux value = new ValueInflux();

        objectRepository.findByObjectIdAndInstanceId(objId, instId).ifPresent(object -> {
            String resID = null;
            resID = event.getN() == null ? path.getName(path.getNameCount() - 1).toString() : event.getN();
            String v = event.getV();
            log.debug("ObjInstance ID: " + objId);
            log.debug("resource ID: " + resID);
            log.info("Value: " + v);
            String finalResID = resID;
            object.getResources().stream()
                    .filter(r -> r.getResourceId().toString().equals(finalResID))
                    .findFirst().ifPresent(resource -> {

                        // value.setValue(v);
                        // value.setResource(resource);


                        //value.setResourceId(resource.id);
                        //value.setValue(v);

                        // TODO gestione del timestamp da fare in futuro
                        // data.setTime(Instant.now());

                        //influxFunctions.writeValue(value);


                //New TEST
                        influxFunctions.writePointValue(resource.id, v, resource.type);
                        // valueRepository.save(value);

                        // this.checkAlarm(resource, path, value);

                        resourceRepository.findOneWithObserversById(resource.getId()).ifPresent(r -> {
                            r.getObservers().forEach(o -> {
                                try {
                                    observerService.sendResourceNotification(o, resource);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            });
                        });
                    });
        });

        //() -> log.info("resource non trovata")
        // I put the return to void, because value is not used
        // return value;
    }

    private void checkAlarm(Resource resource, Path path, Value value) {
        // If Resource ID is NOT Sensor Value (5700), return
        // TODO: Check if resource is not 5700. It could be different.
        if (!resource.getResourceId().equals("5700"))
            return;

        String devId = path.subpath(0, 1).toString();
        String objId = path.subpath(1, 2).toString();
        String instId = path.subpath(2, 3).toString();

        AtomicReference<String> threshold = new AtomicReference<>("NULL");
        AtomicBoolean operator = new AtomicBoolean(false);
        // Check if an Alarm is Set, threshold (6014) isPresent and NOT "NULL"
        resourceRepository
                .findResourceByDeviceObjectInstanceResourceId(devId, objId, instId, "6014")
                .ifPresent(r -> {

                    // Extract the list of allValue
                    List<ValueInflux> allValue = influxFunctions.queryAllDataMod(r.getId().toString());
                    // Check if the list is Empty
                    if (!allValue.isEmpty()) {
                        // Set the threshold
                        threshold.set(allValue.get(0).getValue().toString());
                    }

                    // OLD METHOD
                    /*
                     * valueRepository.findFirstByResourceIdOrderByCreatedAtDesc(r.getId()).
                     * ifPresent(v -> {
                     * threshold.set(v.getValue());
                     * });
                     */
                });

        if (threshold.get().equals("NULL"))
            return;

        // Get Operator for Alarm (6015)
        resourceRepository.findResourceByDeviceObjectInstanceResourceId(devId, objId, instId, "6015")
                .ifPresent(r -> {

                    //// Extract the list of allValue of the r.getID
                    List<ValueInflux> allValue = influxFunctions.queryAllDataMod(r.getId().toString());

                    if (!allValue.isEmpty()) {
                        if (allValue.get(0).getValue().toString().equals("1"))
                            operator.set(true);
                    }

                    // OLD METHOD
                    /*
                     * valueRepository.findFirstByResourceIdOrderByCreatedAtDesc(r.getId()).
                     * ifPresent(v -> {
                     * if (v.getValue().equals("1"))
                     * operator.set(true);
                     * });
                     */
                });

        double thresholdDouble = Double.parseDouble(threshold.get());
        double valueDouble = Double.parseDouble(value.getValue());

        String state = "0";
        if (!operator.get()) // false == 0
            state = valueDouble >= thresholdDouble ? "1" : "0";
        else if (operator.get()) // true == 1
            state = valueDouble <= thresholdDouble ? "1" : "0";

        // Call EventService.notifyRes over /deviceID/objId/instId/6013
        this.notifyRes(new MqttEventDto(null, state), Paths.get(devId, objId, instId, "6013"));
    }

    /*
     * Arriva il payload [{“n”:”resID”,“v”:"value"},{“n”:”
     * resID”,“v”:"value"},{“n”:” resID”,“v”:"value"}]
     * invio agli observer il notify ed invio ogni singola risorsa contenuta nel
     * payload al notifyRes
     */
    private List<Value> notifyObjInst(MqttPayloadDto payload, Path path) {
        String objId = path.subpath(1, 2).toString();
        String instId = path.subpath(2, 3).toString();
        List<Value> values = new ArrayList<>();
        List<MqttEventDto> list_events = payload.getE();

        for (MqttEventDto ev : list_events) {
            log.info("Notifying RES" + ev.toString());
            this.notifyRes(ev, path);

        }

        /* payload.getE().forEach(e -> {
            log.info("Notifying RES" + e.toString());
            this.notifyRes(e, path);
        });

         */

        objectRepository.findWithObserversByObjectIdAndInstanceId(objId, instId).ifPresent(o -> {
            o.getObservers().forEach(observer -> {
                try {
                    observerService.sendObjectInstanceNotification(observer, o);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            /*
             * payload.getE().forEach(e -> {
             * log.info("Notifying RES" + e.toString());
             * this.notifyRes(e, path);
             * });
             */
        });
        return values;
    }

    public List<Event> findAll() {
        return (List<Event>) eventRepository.findAll();
    }

      /* Nuovo codice */
    /* ***************************************************************************************************************************************** */
    /* ***************************************************************************************************************************************** */
    /* ***************************************************************************************************************************************** */

    public void observeResponse(ObserveResponse v, String path, String endpoint) throws IOException {

        String Path [] = path . split ("/") ;

        if(Path.length==3) {

            ObserveObjectResponse resp = new ObserveObjectResponse();
            String respon = this.objectMapper.writeValueAsString(v);

            log.info("Observefrom"+respon);

            String [][] payload = JSONConv.parser(respon, "instance");
            resp.setEvent("NOTIFICATION");
            ObserveObjectData data = new ObserveObjectData();
            ObserveObjectVal val = new ObserveObjectVal();
            val.setId(payload[1][0]) ;
            List <ResourceDto> resourceDtos = new ArrayList<>();

            objectRepository.findByEndpointObjectIdInstanceId(endpoint, Path[1], Path[2]).ifPresent(object -> {

                List <Resource> resource = new ArrayList<>();
                data.setEp(object.getDevice().getEndpoint());
                data.setRes(object.getDevice().getEndpoint()+"/"+object.getObjectId()+"/"+payload[1][0]);
                resource = object.getResources();
                int k = 0;
                int l = k+2;

                while(k<resource.size()) {

                    ResourceDto resourceDto = new ResourceDto();
                    String a = "";
                    String b = "";
                    a = resource.get(k).getResourceId();

                    if(l<payload.length) {
                        b = payload[l][0];
                    }

                    if(a.equals(b)) {

                        resourceDto . setId ( payload [ l ][0]) ;
                        resourceDto . setValue ( payload [ l ][1]) ;
                        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
                        LocalDateTime now = LocalDateTime.now();

                        resourceDto.setTimestamp(dtf.format(now));
                        k++;
                        l++;

                    } else {

                        resourceDto.setId(resource.get(k).getResourceId());
                        k++;

                    }

                    resourceDtos.add(resourceDto);

                }

            });

            val.setResources(resourceDtos);
            data.setVal(val);
            resp.setData(data);

            // XXXXXXXXXXXXXXXXXXXXXXXXXXXXX   PER DISATTIVARE LE NOTIFICHE, COMMENTARE LA RIGA SOTTOSTANTE   XXXXXXXXXXXXXXXXXXXXXXXXXXXXX //
            notifyService.notifyObject(resp);
            // XXXXXXXXXXXXXXXXXXXXXXXXXXXXX   PER DISATTIVARE LE NOTIFICHE, COMMENTARE LA RIGA SOPRASTANTE   XXXXXXXXXXXXXXXXXXXXXXXXXXXXX //
        
        } else if (Path.length==4) {

            ObserveResourceResponse resp = new ObserveResourceResponse();
            String respon = this.objectMapper.writeValueAsString(v);
            String [][] payload = JSONConv.parser(respon,"resource");
            resp.setEvent("NOTIFICATION");
            ObserveResourceData data = new ObserveResourceData();
            ResourceDto resourceDto = new ResourceDto();

            objectRepository.findByEndpointObjectIdInstanceId(endpoint, Path[1], Path[2]).ifPresent( object -> {

                data.setEp(object.getDevice().getEndpoint());
                data.setRes(object.getDevice().getEndpoint() + "/" + object.getObjectId() + "/" + object.getInstanceId() + "/" + payload[0][0]);
                resourceDto.setId(payload[0][0]);
                resourceDto.setValue(payload[0][1]);

                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
                LocalDateTime now = LocalDateTime.now();
                resourceDto.setTimestamp(dtf.format(now));

            });

            data.setVal(resourceDto);
            resp.setData(data);

            // XXXXXXXXXXXXXXXXXXXXXXXXXXXXX   PER DISATTIVARE LE NOTIFICHE, COMMENTARE LE RIGA SOTTOSTANTE   XXXXXXXXXXXXXXXXXXXXXXXXXXXXX //
            notifyService.notifyResource(resp);
            // XXXXXXXXXXXXXXXXXXXXXXXXXXXXX   PER DISATTIVARE LE NOTIFICHE, COMMENTARE LE RIGA SOPRASTANTE   XXXXXXXXXXXXXXXXXXXXXXXXXXXXX //
        }

    }

    /* ----------------------------------------------------------------------------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------------------------------------------------------------------------- */

}