package it.unirc.vo.coap;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JSONConv {

    public static Properties properties = null;
    public static JSONObject jsonObject = null;

    static {
        properties = new Properties();
    }

    public static String[][] parser(String input, String type) {

        try {

            int c;

            switch (type) {

                case "object":
                c = 0;
                break;

                case "instance":
                c = 1;
                break;

                case "resource":
                c = 2;
                break;

                default:
                c = 0;
                break;
            }

            JSONParser jsonParser = new JSONParser();
            Object object = jsonParser.parse(input);
            jsonObject = (JSONObject) object;

            List <String> obj1 = new ArrayList <String>();
            List <String> ins = new ArrayList <String>();
            List <String> res = new ArrayList <String>();
            int values = 0;
            parseJson(jsonObject, c, obj1, ins, res, values);

            int len=2;

            String Final [][] = new String [ins.size()*2+res.size()/2][len];
            int k = 0;

            for(int i = 0; i < ins.size()*2+res.size()/2; i++) {

                for(int j = 0; j < Final[0].length; j++) {

                    if(ins.size() != 0) {

                        if(i == 0 && j == 0) {

                            if(obj1.size() != 0) {
                                Final[i][j] = obj1.get(i);
                            }

                        } else if(i == 1) {

                            if(len != 0) {

                                if(j%2 == 0) {
                                    Final[i][j] = ins.get(j/2);
                                }

                            }

                        } else if(i > 0) {
                            Final[i][j] = res.get(k-2-ins.size()*2);
                        }

                    } else {
                        Final[i][j] = res.get(j);
                    }

                    k++;

                }
            }

            return Final;

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }

    public static int getArray(Object object2, int c, List <String> obj1, List <String> ins, List <String> res, int values) throws ParseException {

        JSONArray jsonArr = (JSONArray) object2;

        for(int k = 0; k < jsonArr.size(); k ++) {

            if(jsonArr.get(k) instanceof JSONObject) {
                parseJson((JSONObject) jsonArr.get(k), c, obj1, ins, res, values);
            } else {
                System.out.println(jsonArr.get(k)+"2");
            }
        }
        return c ;
    }

    public static int parseJson(JSONObject jsonObject, int c, List <String> obj1, List <String> ins, List <String> res, int values) throws ParseException {

        @SuppressWarnings("unchecked")
        Set <Object> set = jsonObject.keySet();
        
        Iterator <Object> iterator = set.iterator();
        
        @SuppressWarnings("unused")
        int c_ite = 0;

        while (iterator.hasNext()) {

            c_ite ++;
            Object obj = iterator.next();

            if(jsonObject.get(obj) instanceof JSONArray) {

                c++;
                getArray(jsonObject.get(obj), c, obj1, ins, res, values);
                c--;

            } else {

                if(jsonObject.get(obj) instanceof JSONObject) {

                    c++;
                    values = parseJson((JSONObject)jsonObject.get(obj), c, obj1, ins, res, values);
                    c--;

                } else {

                    if(c == 1 && obj.toString().equals("id")) {

                        obj1.add(jsonObject.get(obj).toString());

                    } else if(c == 2 && obj.toString().equals("id")) {

                        ins.add(jsonObject.get(obj).toString());

                    } else if(c == 3 && (obj.toString().equals("id") || obj.toString().equals("value"))) {

                        res.add(jsonObject.get(obj).toString());

                        if(values == 1) {

                            String aux = res.get(res.size()-2);
                            res.set(res.size()-2, res.get(res.size()-1));
                            res.set(res.size()-1, aux);
                            values = 0;

                        }

                    } else if(c == 4) {

                        res.add(jsonObject.get(obj).toString() + " " + jsonObject.get(obj));
                        values = 1;
                    }

                }

            }

        }

        return values;

    }

}