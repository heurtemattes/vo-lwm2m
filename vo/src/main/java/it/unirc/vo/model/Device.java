package it.unirc.vo.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
@Entity
@Table(name = "device")
public class Device {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private Long id;

    // The LWM2M Client's unique end point name.
    @Column(unique = true)
    String endpoint;

    String registrationId;

    String address;

    // The address of the LWM2M Server end point the client used to register.
    String registrationEndpointAddress;

    long lifetime;

    String smsNumber;

    String lwM2mVersion;

    String bindingMode;

    // The location where LWM2M objects are hosted on the device
    String rootPath;

    String resourceType;

    @OneToMany(mappedBy = "device", cascade = CascadeType.ALL)
    List<Object> objectLinks;

    boolean secure;

    public String additionalRegistrationAttributes;

    @CreationTimestamp
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    @UpdateTimestamp
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;

    public void setObjectLinks(List<Object> objectLinks) {
        for (Object child : objectLinks) {
            child.setDevice(this);
        }
        this.objectLinks = objectLinks;
    }
}


	
