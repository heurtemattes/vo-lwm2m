package it.unirc.vo.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
@Entity
@Table(name = "object", uniqueConstraints =
@UniqueConstraint(columnNames = {"objectId", "instanceId"})
)
public class Object extends Observable {
    private String objectId;
    private String instanceId;
    private String description;
    private String mandatory;

    @LazyCollection(LazyCollectionOption.FALSE) /* Nuovo codice ***************************************************************************************************************************************** */
    @OneToMany(mappedBy = "object", cascade = CascadeType.ALL)
    private List<Resource> resources;

    @ManyToOne
    @JoinColumn(name = "device_id", referencedColumnName = "id")
    @JsonIgnore
    private Device device;

    @CreationTimestamp
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    @UpdateTimestamp
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;

    public void setResources(List<Resource> resources) {
        for (Resource child : resources) {
            child.setObject(this);
        }
        this.resources = resources;
    }
}
