package it.unirc.vo.model;

import com.influxdb.annotations.Column;
import com.influxdb.annotations.Measurement;

import java.time.Instant;

//POJO object used to store data retrieved from DB
@Measurement(name = "Value")
public class ValueInflux {
    @Column(tag = true)
    String resourceID; // resourceId


    @Column(timestamp = true)
    Instant time; // 2023-03-12

    @Column (tag = true)
    String type;

    @Column (name = "valueFloat")
    Double valueFloat; // used to store float values (influx use float64, in java is double)

    @Column
    String valueString; //used to store and query String values

    @Column
    Long valueInteger; //used to store and query Integer values (influx use Int64 to store integer, so in java is Long)

    @Column
    Instant valueTime; //used to store and query Time values

    @Column
    boolean valueBoolean; //used to store and query Boolean values

    @Column
    String valueDefault; //used to store and query Default values

    public Instant getTime() {
        return time;
    }

    public String getValue() {
        switch (type){
            case "String": return valueString;
            case "Float": return String.valueOf(valueFloat);
            case "Time": return valueTime.toString();
            case "Boolean": return String.valueOf(valueBoolean);
            case "Integer": return String.valueOf(valueInteger);
            default: return valueDefault;
        }
    }

    public String getType() {
        return type;
    }

    public String getResourceID() {
        return resourceID;
    }

    public void setResourceId(Long resourceId) {
        this.resourceID = resourceId.toString();
    }

    public void setResourceId(String resourceId) {
        this.resourceID = resourceId;
    }

    public void setTime(Instant time) {
        this.time = time;
    }

    public void setValue(String value) {
        switch (type){
            case "String": this.valueString = value;
                break;
            case "Float":
                try{
                    this.valueFloat = Double.parseDouble(value);
                } catch (Exception e) {
                    System.out.println("Exception in parsing Float values: " + e.getMessage());
                }
                break;
            case "Time":
                try {
                    this.valueTime = Instant.parse(value); //Registered as a String
                } catch (Exception e) {
                    System.out.println("Exception in parsing Instant values: " + e.getMessage());
                }
                    break;
            case "Boolean":
                try{
                    this.valueBoolean = Boolean.parseBoolean(value);
                } catch (Exception e) {
                    System.out.println("Exception in parsing Boolean values: " + e.getMessage());
                }
                break;
            case "Integer":
                try{
                    this.valueInteger = Long.parseLong(value);
                } catch (Exception e) {
                    System.out.println("Exception in parsing Integer values: " + e.getMessage());
                }
                break;
            default: this.valueDefault = value;
                break;
        }

    }

    @Override
    public String toString() {
        return "ValueInflux{" +
                "resourceID='" + resourceID + '\'' +
                ", time=" + time +
                ", type='" + type + '\'' +
                ", valueFloat=" + valueFloat +
                ", valueString='" + valueString + '\'' +
                ", valueInteger=" + valueInteger +
                ", valueTime=" + valueTime +
                ", valueBoolean=" + valueBoolean +
                ", valueDefault='" + valueDefault + '\'' +
                '}';
    }
}// POJO
