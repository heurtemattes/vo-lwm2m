package it.unirc.vo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Observable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    public Long id;

    public String name;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "observable", cascade = CascadeType.ALL)
    private List<Observer> observers;

    public void addObserver(Observer observer) {
        observer.setObservable(this);
        this.observers.add(observer);
    }
}
