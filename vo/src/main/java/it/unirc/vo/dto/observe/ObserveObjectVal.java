package it.unirc.vo.dto.observe;

import lombok.Data;

import java.util.List;

import it.unirc.vo.dto.ResourceDto;

@Data
public class ObserveObjectVal {
    String id;
    List<ResourceDto> resources;
}
