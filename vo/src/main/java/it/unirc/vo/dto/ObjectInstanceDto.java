package it.unirc.vo.dto;

import lombok.Data;

import java.util.List;

@Data
public class ObjectInstanceDto {
    String id;
    List<ResourceDto> resources;
}
