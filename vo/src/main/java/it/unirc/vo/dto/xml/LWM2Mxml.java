package it.unirc.vo.dto.xml;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class LWM2Mxml {
    String noNamespaceSchemaLocation;
    @JsonProperty("Object")
    Objectxml Object;
}
