package it.unirc.vo.dto;

import lombok.Data;

import java.util.List;

@Data
public class ContentResourceDto {
    String id;
    List<ResourceDto> resources;
}
