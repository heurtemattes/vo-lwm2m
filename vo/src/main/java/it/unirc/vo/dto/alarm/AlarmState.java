package it.unirc.vo.dto.alarm;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AlarmState {

    @JsonProperty("state")
    private String state;

    @JsonProperty("threshold")
    private String threshold;

    @JsonProperty("operator")
    private String operator;
}
