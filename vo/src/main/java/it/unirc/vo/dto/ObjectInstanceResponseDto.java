package it.unirc.vo.dto;

import lombok.Data;

@Data
public class ObjectInstanceResponseDto {
    String status;
    ContentResourceDto content;
}
