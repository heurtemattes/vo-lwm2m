package it.unirc.vo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MqttPublishResource {
    String v;
}
