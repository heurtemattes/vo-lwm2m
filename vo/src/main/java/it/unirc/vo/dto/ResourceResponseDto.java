package it.unirc.vo.dto;

import lombok.Data;

@Data
public class ResourceResponseDto {
    String status;
    ResourceDto content;
}
