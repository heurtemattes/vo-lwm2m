package it.unirc.vo.dto.observe;

import lombok.Data;

@Data
public class ObserveObjectData {
    String ep;
    String res;
    ObserveObjectVal val;
}
