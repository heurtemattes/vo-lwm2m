package it.unirc.vo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.unirc.vo.model.Observable;

@Repository
public interface ObservableRepository extends CrudRepository<Observable, Long> {
}
