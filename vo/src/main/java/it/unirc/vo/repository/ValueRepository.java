package it.unirc.vo.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import it.unirc.vo.model.Value;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface ValueRepository extends CrudRepository<Value, Long> {
    Optional<Value> findFirstByResourceIdOrderByCreatedAtDesc(Long resourceId);

    List<Value> findByResourceIdAndValueOrderByCreatedAtDesc(Long resourceId, String value);

    List<Value> findAll(Pageable pageable);

    List<Value> findAllByResourceId(Long resourceId, Pageable pageable);

    List<Value> findByResourceIdAndCreatedAtBetweenOrderByCreatedAtDesc(Long resourceId, Date startDate, Date endDate);

    @Query(nativeQuery = true,
            value = "SELECT * FROM Value AS v WHERE v.resource_id = :r_id AND CAST(v.value as real) > CAST(:value AS real) ORDER BY created_at DESC")
    List<Value> findByResourceIdAndValueMoreThanOrderByCreatedAtDesc(@Param("r_id") Long resourceId, @Param("value") String value); //da rimappare

    @Query(nativeQuery = true,
            value = "SELECT * FROM Value AS v WHERE v.resource_id = :r_id AND CAST(v.value as real) < CAST(:value AS real) ORDER BY created_at DESC")
    List<Value> findByResourceIdAndValueLessThanOrderByCreatedAtDesc(@Param("r_id") Long resourceId, @Param("value") String value); //da rimappare

    @Query(nativeQuery = true,
        value = "SELECT * FROM Value AS v WHERE v.resource_id = :r_id ORDER BY created_at DESC")
        List<Value> findByResourceId (@Param ("r_id") String resourceId); //da rimappare

    // List<Value> findById (Long resourceId);


}
