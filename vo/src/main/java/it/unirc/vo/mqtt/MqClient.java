package it.unirc.vo.mqtt;

import it.unirc.vo.service.EventService;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MqttDefaultFilePersistence;
import org.springframework.beans.factory.annotation.Autowired;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
@NoArgsConstructor
public class MqClient implements MqttCallback, IMqttMessageListener { //IMqttMessageListener interface added to handle manifold mqtt messages
    private static final int DEVICE = 2;
    private static final int OBJECT = 3;
    private static final int INSTANCE = 4;
    private static final int RESOURCE = 5;
    public static final String OBSERVATION = "tele";
    public static final String NOTIFICATION = "stat";
    public static final String COMMAND = "cmnd";
    private static final String DEREGISTRATION = "dereg";

    //Realization of MQTT AsyncClient
    private MqttAsyncClient client; 
    
    private String brokerUrl;
    private String clientId;
    private MqttConnectOptions conOpt;
    private Boolean cleanSession = false;
    private Throwable ex = null;
    private String password;
    private String userName;
    
    //Threads pool creation. Used to handle manifold mqtt messages
    private ExecutorService pool = Executors.newFixedThreadPool(10); 

    @Autowired
    EventService eventService;

    /**
     * Constructs an instance of the sample client wrapper
     *
     * @param brokerUrl    the url to connect to
     * @param clientId     the client id to connect with
     * @param cleanSession clear state at end of connection or not (durable or
     *                     non-durable subscriptions)
     * @param userName     the username to connect with
     * @param password     the password for the user
     * @throws MqttException
     */
    public MqClient(String brokerUrl, String clientId, boolean cleanSession, String userName, String password)
            throws MqttException {
        this.brokerUrl = brokerUrl;
        this.cleanSession = cleanSession;
        this.password = password;
        this.userName = userName;
        this.clientId = clientId;

        // This sample stores in a temporary directory... where messages temporarily
        // stored until the message has been delivered to the server.
        // ..a real application ought to store them somewhere
        // where they are not likely to get deleted or tampered with
        String tmpDir = System.getProperty("java.io.tmpdir");
        MqttDefaultFilePersistence persistence = new MqttDefaultFilePersistence(tmpDir);

        try {
            client = new MqttAsyncClient(this.brokerUrl, this.clientId, persistence); //AsyncClient uses non-blocking methods
            conOpt = new MqttConnectOptions();
            conOpt.setCleanSession(this.cleanSession);
            conOpt.setAutomaticReconnect(true);
            if (password != null) {
                conOpt.setPassword(this.password.toCharArray());
            }
            if (userName != null) {
                conOpt.setUserName(this.userName);
            }
            // !!!!!!!!!
            // TODO optimize last Will Message
            String lastwillmsg = "client losts connection" + clientId;
            conOpt.setWill("lastWill", lastwillmsg.getBytes(), 2, false);

            // Set this wrapper as the callback handler
            client.setCallback(this);
            MqttConnector con = new MqttConnector();
            con.doConnect();

        } catch (MqttException e) {
            e.printStackTrace();
            log.info("Unable to set up client: " + e.toString());
            System.exit(1);
        }

    }

    public void publish(String topicName, int qos, byte[] payload, boolean retained) throws Throwable {
        Publisher pub = new Publisher();
        pub.doPublish(topicName, qos, payload, retained);
    }

    public void subscribe(String topicName, int qos) throws Throwable {
        Subscriber sub = new Subscriber();
        sub.doSubscribe(topicName, qos);
    }

    public class Publisher {
        /**
         * Publish / send a message to an MQTT server
         *
         * @param topicName the name of the topic to publish to
         * @param qos       the quality of service to delivery the message at (0,1,2)
         * @param payload   the set of bytes to send to the MQTT server
         * @throws MqttException
         */
        public void doPublish(String topicName, int qos, byte[] payload, boolean retained) throws Throwable {

            String time = new Timestamp(System.currentTimeMillis()).toString();
            log.info("Publishing at: " + time + " to topic \"" + topicName + "\" qos " + qos);

            MqttMessage message = new MqttMessage(payload);
            message.setQos(qos);
            if (retained) {
                // If retained is true. Default value is false.
                message.setRetained(retained);
            }
            IMqttActionListener pubListener = new IMqttActionListener() {
                public void onSuccess(IMqttToken asyncActionToken) {
                    log.info("Publish Completed");
                }

                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    ex = exception;
                    log.info("Publish failed" + exception);

                }
            };

            try {
                // Publish the message
                client.publish(topicName, message);
                // client.publish(topicName, payload, qos, retained);
            } catch (MqttException e) {
                ex = e;
                log.error("Error sending the messagge" + ex);
            }
        }

    }

    public class Subscriber {

        public void doSubscribe(String topicName, int qos) {
            // Make a subscription
            // Get a token and setup an asynchronous listener on the token which
            // will be notified once the subscription is in place.
            log.info("Subscribing to topic \"" + topicName + "\" qos " + qos);

            IMqttActionListener subListener = new IMqttActionListener() {
                public void onSuccess(IMqttToken asyncActionToken) {
                    log.info("Subscribe Completed");
                }

                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    ex = exception;
                    log.info("Subscribe failed" + exception);
                }
            };
            try {
                client.subscribe(topicName, qos);
            } catch (MqttException e) {
                ex = e;
                log.info("Subscribe failed" + e);
            }
        }
    }

    /**
     * Connect in a non-blocking way and then sit back and wait to be notified that
     * the action has completed.
     */
    public class MqttConnector {

        public MqttConnector() {
        }

        public void doConnect() {
            // Connect to the server
            // Get a token and setup an asynchronous listener on the token which
            // will be notified once the connect completes
            log.info("Connecting to " + brokerUrl + " with client ID " + client.getClientId());

            IMqttActionListener conListener = new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    log.info("Connected");
                    // state = CONNECTED;
                    // carryOn();
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    ex = exception;
                    // state = ERROR;
                    log.info("connect failed" + exception);
                    // carryOn();
                }
            };
            try {
                //Connect using a non-blocking connect. Asynchronicity implies waiting for client connection
                client.connect(conOpt).waitForCompletion(); //client.connect(conOpt) 
            } catch (MqttException e) {
                // If though it is a non-blocking connect an exception can be
                // thrown if validation of parms fails or other checks such
                // as already connected fail.
                ex = e;
            }
        }
    }

    @Override
    public void connectionLost(Throwable cause) {
        log.info("Connection to " + brokerUrl + " lost!" + cause);
    }


    //Handling manifold mqtt messages
    class MessageHandler implements Runnable {
        MqttMessage message;
        String topic;

        public MessageHandler(String topic, MqttMessage message) {
            this.message = message;
            this.topic = topic;
        }

        public void run() {
            checkMessageType(topic, message); //Uses DB, bottleneck
        }
    }

    //messageArrived(...) modified to handle manifold mqtt messages
    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        try {
            pool.execute(new MessageHandler(topic, message));
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }

    }

    //messageArrived(...)
    // @Override
    // public void messageArrived(String topic, MqttMessage message) throws
    // Exception {
    // log.info("MQTT message arrived on topic:" + topic + "; Payload: " + new
    // String(message.getPayload()) + "; QoS: " + message.getQos());
    // this.checkMessageType(topic, message);
    // }

    /*
     * Need 2 checks:
     * 1-check topic's tree deep (2,3,4)
     * //i.e. PayloadType(deep):
     * //Resource (4): stat/deviceID/objId/instId/resId
     * //Instance (3): stat/deviceID/objId/instId
     * //Object (2): stat/deviceID/objId
     * 
     * 2-check Message type:
     * cmnd: used to send command to the device (theorically, the VO should not
     * receive them)
     * stat: Notification
     * tele: Notification to observe activation
     * 
     * if prefix is "tele", you are receiving a periodic response (observe) with
     * data:
     * if prefix is "stat", you are receiving a response
     */

    public void checkMessageType(String topic, MqttMessage message) {
        // get path
        Path p = Paths.get(topic);
        String prefix = p.subpath(0, 1).toString();
        if (prefix.equals(OBSERVATION) || prefix.equals(NOTIFICATION)) {
            // convert message.payload to Json
            String payload = new String(message.getPayload(), StandardCharsets.UTF_8);
            Path path = p.subpath(1, p.getNameCount());
            log.info("Path to string" + path.toString());
            log.info(message.toString());
            try {
                String resp = eventService.notification(path, payload);
                log.info(resp);
            } catch (Exception ex) {
                log.error("Notification Error {}", ex.getMessage());
            }
        } else if (prefix.equals(COMMAND)) {
            // log.error("Message Topic-Prefix not allowed: %s", prefix);
            log.info("Message Topic-Prefix not implemented(" + prefix + "). Used only for Physical device commands");
        } else {
            log.info("Message Topic-Prefix unknown:" + prefix);
        }
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
    }

}