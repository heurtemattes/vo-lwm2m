package it.unirc.vo.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;


//<------------------------COAP Import------------------------>
import it.unirc.vo.coap.CoapClient;
import it.unirc.vo.coap.CoapClientBuilder;
import it.unirc.vo.selection.CoAPCondition;
import it.unirc.vo.selection.MQTTCondition;

import org.eclipse.leshan.core.observation.Observation;
import org.eclipse.leshan.server.registration.Registration;
import org.eclipse.leshan.server.registration.RegistrationListener;
import org.eclipse.leshan.server.registration.RegistrationUpdate;
import org.eclipse.paho.client.mqttv3.MqttException;

import org.springframework.context.annotation.Conditional;

//<---------------------------------------------------------->
import lombok.extern.slf4j.Slf4j;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

import okhttp3.logging.HttpLoggingInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import it.unirc.vo.dto.device.DeviceResponse;
import it.unirc.vo.dto.xml.LWM2Mxml;
import it.unirc.vo.model.Device;
import it.unirc.vo.model.Object;
import it.unirc.vo.mqtt.MqClient;
import it.unirc.vo.service.CompositeService;
import it.unirc.vo.service.DeviceService;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@Slf4j
@Configuration
public class AppConfig {
    private final RegistrationConfig registrationConfig;
    private final CompositeConfig compositeConfig;
    private final DeviceService deviceService;
    private boolean composite = false;

    public AppConfig(@Autowired RegistrationConfig registrationConfig, @Autowired DeviceService deviceService,
            @Autowired CompositeConfig compositeConfig) {
        this.deviceService = deviceService;
        this.registrationConfig = registrationConfig;
        this.compositeConfig = compositeConfig;

        // Check Path
        String pathclass = AppConfig.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        String decodedPath = null;
        try {
            decodedPath = URLDecoder.decode(pathclass, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // Display Conf parms
        log.info("Execution folder:" + decodedPath);
        String urlConf = null;
        if (this.registrationConfig.getUrl() != null) {
            urlConf = this.registrationConfig.getUrl();
        }
        log.info("Url:" + urlConf);
        String objLinkConf = null;
        if (this.registrationConfig.getObjectLinks() != null) {
            objLinkConf = this.registrationConfig.getObjectLinks().toString();
        }
        log.info("ObjectLinks:" + objLinkConf);

        if (this.registrationConfig.getDevice() != null) {
            Device device = registrationConfig.getDevice();
            log.info("Device {}:", device.toString());
            if (!deviceService.findByEndpoint(device.getEndpoint()).isPresent()) {
                log.info("Device not found. Creating new one from registration.yaml: {}", device.toString());
                device.setObjectLinks(registrateObjects(registrationConfig.getObjectLinks()));
                deviceService.create(device);
            }
            log.info("Binding mode: " + device.getBindingMode());
        } else if (this.compositeConfig.getDevices() != null) {
            log.info("Composite VO found. Creating new devices from registration.yaml: {}",
                    compositeConfig.getDevices().toString());
            this.composite = true;
            this.compositeConfig.getDevices().forEach(c -> {
                // Skip iteration if device found on DB
                if (deviceService.findByEndpoint(c.get("id").toString()).isPresent())
                    return;

                HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                logging.setLevel(HttpLoggingInterceptor.Level.NONE);

                OkHttpClient.Builder httpClient = new OkHttpClient.Builder().addInterceptor(logging);

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("http://" + c.get("ip").toString())
                        .addConverterFactory(JacksonConverterFactory.create())
                        .client(httpClient.build())
                        .build();

                CompositeService service = retrofit.create(CompositeService.class);
                Call<DeviceResponse> callSync = service.getUser(c.get("id").toString());

                try {
                    Response<DeviceResponse> response = callSync.execute();
                    DeviceResponse deviceResponse = response.body();
                    if (deviceResponse != null) {
                        Device device = deviceResponseToDeviceModel(deviceResponse);
                        device.setObjectLinks(registrateObjects(deviceResponseToObjectLinks(deviceResponse)));
                        device.setAddress(c.get("ip").toString());
                        deviceService.create(device);
                    }
                    log.info("Binding mode: " + deviceResponse.getBindingMode());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                log.info("Registering CVO as Observer to VO: ID:" + c.get("id").toString() + " IP:"
                        + c.get("ip").toString());
                String cvoServiceSuffix = "-service";
                String cvoAddr = this.compositeConfig.getEndpoint().concat(cvoServiceSuffix);
                Call<ResponseBody> callRegister = service
                        .registerCvo(RequestBody.create(MediaType.parse("text/plain"), cvoAddr));
                try {
                    Response<ResponseBody> regResponse = callRegister.execute();
                    log.info(regResponse.toString());
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            });
        } else if (this.registrationConfig.getDevice() == null) {
            log.error("Device value is null. Check Configuratione file");
        }

    }

    private Device deviceResponseToDeviceModel(DeviceResponse dr) {
        Device d = new Device();
        d.setEndpoint(dr.getEndpoint());
        d.setBindingMode(dr.getBindingMode());
        d.setLwM2mVersion(dr.getVersion());
        d.setRegistrationId(dr.getRegistrationId());
        d.setLifetime(dr.getLifetime());
        d.setRootPath(dr.getRootPath());
        d.setSecure(dr.isSecure());
        return d;
    }

    private List<String> deviceResponseToObjectLinks(DeviceResponse deviceResponse) {
        List<String> objectLinks = new ArrayList<>();
        deviceResponse.getObjectLinks().forEach(o -> objectLinks.add(o.getUrl().replaceFirst("/", "")));
        objectLinks.removeAll(Arrays.asList("", null));
        return objectLinks;
    }

    private List<Object> registrateObjects(List<String> objectLinks) {
        List<Object> resp = new ArrayList<>();
        objectLinks.forEach(object -> {
            log.info("Object: " + object);
            String[] parts = object.split("/");
            if (parts.length > 0) {
                Resource resource = new ClassPathResource("models/" + parts[0] + ".xml");
                try {
                    InputStream input = resource.getInputStream();
                    ObjectMapper xmlMapper = new XmlMapper();
                    LWM2Mxml lwm2Mxml = xmlMapper.readValue(input, LWM2Mxml.class);
                    resp.add(this.lwm2mToEntiy(lwm2Mxml, parts[1]));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                log.info("Incorrect ObjectLink in config");
            }
        });
        return resp;
    }

    private Object lwm2mToEntiy(LWM2Mxml lwm2Mxml, String part) {
        Object object = new Object();
        List<it.unirc.vo.model.Resource> resources = new ArrayList<>();
        object.setObjectId(lwm2Mxml.getObject().getObjectID().toString());
        object.setInstanceId(part);
        object.setName(lwm2Mxml.getObject().getName());
        object.setDescription(lwm2Mxml.getObject().getDescription1());
        object.setMandatory(lwm2Mxml.getObject().getMandatory());
        lwm2Mxml.getObject().getResources().forEach(r -> {
            it.unirc.vo.model.Resource rr = new it.unirc.vo.model.Resource();
            rr.setResourceId(r.getID());
            rr.setName(r.getName());
            rr.setOperations(r.getOperations());
            rr.setInstanceType(r.getMultipleInstances());
            rr.setMandatory(r.getMandatory().equals("Mandatory"));
            rr.setType(r.getType());
            rr.setUnits(r.getUnits());
            rr.setDescription(r.getDescription());
            resources.add(rr);
        });
        object.setResources(resources);
        return object;
    }

    @Bean
    @Conditional(MQTTCondition.class)
    public MqClient mqClient() {
        MqClient mqClient = new MqClient();

        if (compositeConfig.getDevices() != null)
            return mqClient;

        String devEndp = registrationConfig.getDevice().getEndpoint();
        try {
            mqClient = new MqClient(
                    registrationConfig.getUrl(),
                    devEndp,
                    registrationConfig.isCleanSession(),
                    registrationConfig.getUsername(),
                    registrationConfig.getPassword());
        } catch (MqttException me) {
            log.error("reason " + me.getReasonCode());
            log.error("msg " + me.getMessage());
            log.error("loc " + me.getLocalizedMessage());
            log.error("cause " + me.getCause());
            log.error("System exit, excep " + me);
            me.printStackTrace();
            System.exit(1);
        }
        try {
            mqClient.subscribe("cmnd/" + devEndp + "/" + "#", registrationConfig.getMqttQos());
        } catch (Throwable e1) {
            log.error("Error subscribing to cmnd/ topic..." + e1);
            e1.printStackTrace();
        }
        try {
            mqClient.subscribe("stat/" + devEndp + "/" + "#", registrationConfig.getMqttQos());
        } catch (Throwable e) {
            log.error("Error subscribing to stat/ topic..." + e);
            e.printStackTrace();
        }
        try {
            mqClient.subscribe("tele/" + devEndp + "/" + "#", registrationConfig.getMqttQos());
        } catch (Throwable e) {
            log.error("Error subscribing to tele/ topic..." + e);
            e.printStackTrace();
        }
        return mqClient;
    }

    @Bean
    @Conditional(CoAPCondition.class)
    public CoapClient Coapclient() {
    	CoapClientBuilder builder = new CoapClientBuilder();
    	
    	//CoapClient Coapclient = new CoapClient();
    	
    	builder.setLocalAddress("localhost", 5683);
    	
        // Handle help or version command
        //if (command.isUsageHelpRequested() || command.isVersionHelpRequested())
            //System.exit(0);

        //try {
            // Create LWM2M Server
            CoapClient lwm2mServer = builder.build();

            // Create Web Server
           // Server webServer = createJettyServer(cli, lwm2mServer);

            // Register a service to DNS-SD
            
            // Start servers
            lwm2mServer.start();
            //webServer.start();
           // LOG.info("Web server started at {}.", webServer.getURI());
            
            lwm2mServer.getRegistrationService().addListener(new RegistrationListener() {

                public void registered(Registration registration, Registration previousReg,
                        Collection<Observation> previousObsersations) {
                    log.info("new device: " + registration.getEndpoint());
                }

                public void updated(RegistrationUpdate update, Registration updatedReg, Registration previousReg) {
                    log.info("device is still here: " + updatedReg.getEndpoint());
                }

                public void unregistered(Registration registration, Collection<Observation> observations, boolean expired,
                        Registration newReg) {
                    log.info("device left: " + registration.getEndpoint());
                }
            });
            
            
           lwm2mServer.Listener();

        //} catch (Exception e) {

            // Handler Execution Error
            //PrintWriter printer = command.getErr();
           // log.error("Unable to create and start server ...");
            //printer.printf("%n%n");
            //printer.print(command.getColorScheme().stackTraceText(e));
            //printer.flush();
            //System.exit(1);
        //}
        return lwm2mServer;
    }

    @Bean
    ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

    @Bean
    boolean isComposite() {
        return composite;
    }
}
