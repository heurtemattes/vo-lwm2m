package it.unirc.vo.controller;

//import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import it.unirc.vo.dto.*;
import it.unirc.vo.dto.device.DeviceResponse;
import it.unirc.vo.dto.observe.ObserveObjectResponse;
import it.unirc.vo.dto.observe.ObserveResourceResponse;
import it.unirc.vo.service.ClientServiceM;
import lombok.extern.log4j.Log4j2;

import javax.servlet.http.HttpServletRequest;

/* Nuovi import */
/* ***************************************************************************************************************************************** */
/* ***************************************************************************************************************************************** */
/* ***************************************************************************************************************************************** */

import it.unirc.vo.selection.MQTTCondition;
import org.springframework.context.annotation.Conditional;
import it.unirc.vo.service.ClientServiceM;

/* ----------------------------------------------------------------------------------------------------------------------------------------- */
/* ----------------------------------------------------------------------------------------------------------------------------------------- */
/* ----------------------------------------------------------------------------------------------------------------------------------------- */





@Log4j2
@RestController
@Conditional(MQTTCondition.class) /* Nuovo codice ***************************************************************************************************************************************** */
@RequestMapping("/api/clients")
public class ClientControllerM {

    private final ClientServiceM clientServiceM;

    @Autowired
    public ClientControllerM(ClientServiceM clientServiceM) {
        this.clientServiceM = clientServiceM;
    }

    @GetMapping("/{endpoint}")
    public DeviceResponse getDeviceResponse(
            @PathVariable("endpoint") String endpoint) {
        return clientServiceM.getDeviceResponse(endpoint);
    }

    @GetMapping("/{endpoint}/{objectId}")
    public ObjectResponseDto getObjectResponse(
            @PathVariable("endpoint") String endpoint,
            @PathVariable("objectId") String objectId) {
        return clientServiceM.getObjectResponse(endpoint, objectId);
    }

    @GetMapping("/{endpoint}/{objectId}/{instanceId}")
    public ObjectInstanceResponseDto getObjectInstanceResponse(
            @PathVariable("endpoint") String endpoint,
            @PathVariable("objectId") String objectId,
            @PathVariable("instanceId") String instanceId,
            HttpServletRequest request) {
        return clientServiceM.getObjectInstanceResponse(endpoint, objectId, instanceId, request);
    }

    @GetMapping("/{endpoint}/{objectId}/{instanceId}/{resourceId}")
    public ResourceResponseDto getResourceResponse(
            @PathVariable("endpoint") String endpoint,
            @PathVariable("objectId") String objectId,
            @PathVariable("instanceId") String instanceId,
            @PathVariable("resourceId") String resourceId,
            @RequestParam(required = false) boolean getRealtime,
            HttpServletRequest request) {
        return clientServiceM.getResourceResponse(endpoint, objectId, instanceId, resourceId, request, getRealtime);
    }

    @PostMapping("/{endpoint}/{objectId}/{instanceId}/observe")
    public ObserveObjectResponse observeObjectInstance(
            @PathVariable("endpoint") String endpoint,
            @PathVariable("objectId") String objectId,
            @PathVariable("instanceId") String instanceId,
            @RequestBody(required = false) String obsAddr,
            HttpServletRequest request) {
        return clientServiceM.observeObjectInstance(endpoint, objectId, instanceId, obsAddr, request);
    }

    @PostMapping("/{endpoint}/{objectId}/{instanceId}/{resourceId}/observe")
    public ObserveResourceResponse observeResource(
            @PathVariable("endpoint") String endpoint,
            @PathVariable("objectId") String objectId,
            @PathVariable("instanceId") String instanceId,
            @PathVariable("resourceId") String resourceId,
            @RequestBody(required = false) String obsAddr,
            HttpServletRequest request) {
        return clientServiceM.observerResource(endpoint, objectId, instanceId, resourceId, obsAddr, request);
    }

    @DeleteMapping("/{endpoint}/{objectId}/{instanceId}/observe")
    public ResponseEntity<String> deleteObserveObjectInstance(
            @PathVariable("endpoint") String endpoint,
            @PathVariable("objectId") String objectId,
            @PathVariable("instanceId") String instanceId,
            @RequestBody(required = false) String obsAddr,
            HttpServletRequest request) {
        clientServiceM.deleteObserveObjectInstance(endpoint, objectId, instanceId, obsAddr, request);
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @DeleteMapping("/{endpoint}/{objectId}/{instanceId}/{resourceId}/observe")
    public ResponseEntity<String> deleteObserveResource(
            @PathVariable("endpoint") String endpoint,
            @PathVariable("objectId") String objectId,
            @PathVariable("instanceId") String instanceId,
            @PathVariable("resourceId") String resourceId,
            @RequestBody(required = false) String obsAddr,
            HttpServletRequest request) {
        clientServiceM.deleteObserverResource(endpoint, objectId, instanceId, resourceId, obsAddr, request);
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @PutMapping("/{endpoint}/{objectId}/{instanceId}")
    public ResponseEntity<String> writeObjectInstance(
            @PathVariable("endpoint") String endpoint,
            @PathVariable("objectId") String objectId,
            @PathVariable("instanceId") String instanceId,
            @RequestBody ObjectInstanceDto objectInstanceDto) throws Throwable {
        clientServiceM.writeObjectInstance(endpoint, objectId, instanceId, objectInstanceDto);
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @PutMapping("/{endpoint}/{objectId}/{instanceId}/{resourceId}")
    public ResponseEntity<String> writeResource(
            @PathVariable("endpoint") String endpoint,
            @PathVariable("objectId") String objectId,
            @PathVariable("instanceId") String instanceId,
            @PathVariable("resourceId") String resourceId,
            @RequestBody ResourceDto resourceDto) throws Throwable {
        clientServiceM.writeResource(endpoint, objectId, instanceId, resourceId, resourceDto);
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @PostMapping("/{endpoint}/{objectId}/{instanceId}/{resourceId}")
    public ResponseEntity<String> executeCommand(
            @PathVariable("endpoint") String endpoint,
            @PathVariable("objectId") String objectId,
            @PathVariable("instanceId") String instanceId,
            @PathVariable("resourceId") String resourceId,
            HttpServletRequest request) throws Throwable {
        clientServiceM.executeCommand(endpoint, objectId, instanceId, resourceId, request);
        return new ResponseEntity<>("", HttpStatus.OK);
    }
}
