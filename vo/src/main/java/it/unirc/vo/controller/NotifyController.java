package it.unirc.vo.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.unirc.vo.dto.observe.ObserveObjectResponse;
import it.unirc.vo.dto.observe.ObserveResourceResponse;
import it.unirc.vo.service.NotifyService;

@Log4j2
@RestController
@RequestMapping("/api/notify")
public class NotifyController {
    private final ObjectMapper objectMapper;
    private final NotifyService notifyService;

    @Autowired
    public NotifyController(ObjectMapper objectMapper,
                            NotifyService notifyService) {
        this.notifyService = notifyService;
        this.objectMapper = objectMapper;
    }

    @PostMapping
    public ResponseEntity<String> notify(@RequestBody String inputJson) throws JsonProcessingException {
        log.info("Notify JSON: {}", inputJson);
        if (checkJsonCompatibility(inputJson, ObserveResourceResponse.class)) {

            ObserveResourceResponse resourceResponse = objectMapper.readValue(inputJson, ObserveResourceResponse.class);
            notifyService.notifyResource(resourceResponse);

        } else {

            ObserveObjectResponse objectResponse = objectMapper.readValue(inputJson, ObserveObjectResponse.class);
            notifyService.notifyObject(objectResponse);
        }
        // Always returns 200 OK; inserire la riga sottostante dentro gli If-Else
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    public boolean checkJsonCompatibility(String jsonStr, Class<?> valueType) {
        try {
            objectMapper.readValue(jsonStr, valueType);
            return true;
        } catch (JsonProcessingException e) {
            return false;
        }
    }
}
