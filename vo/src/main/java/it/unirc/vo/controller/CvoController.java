package it.unirc.vo.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import it.unirc.vo.service.CvoService;

import javax.servlet.http.HttpServletRequest;

@Log4j2
@RestController
@RequestMapping("/api/cvo")
public class CvoController {
    private final CvoService cvoService;

    @Autowired
    public CvoController (CvoService cvoService){
        this.cvoService = cvoService;  
    }
    
    @PostMapping("/register")
    public ResponseEntity<String> cvoRegistration(HttpServletRequest request, @RequestBody String obsAddr){
        return cvoService.registerCvo (request, obsAddr);
    }

    @DeleteMapping("/register")
    public ResponseEntity<String> deleteCvoRegistration(HttpServletRequest request, @RequestBody String obsAddr){
        return cvoService.deleteRegisterCvo (request, obsAddr);
    }
}
