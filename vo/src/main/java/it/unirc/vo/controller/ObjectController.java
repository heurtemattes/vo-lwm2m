package it.unirc.vo.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.unirc.vo.model.Object;
import it.unirc.vo.repository.ObjectRepository;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Log4j2
@RestController
@RequestMapping("/api/objects")
public class ObjectController {

    private final ObjectRepository objectRepository;

    @Autowired
    public ObjectController(ObjectRepository objectRepository) {
        this.objectRepository = objectRepository;
    }

    @GetMapping
    public List<Object> findAll() {
        return (List<Object>) objectRepository.findAll();
    }

    @GetMapping("/{objectId}/{instanceId}")
    public Object findByObjectIdAndInstanceId(@PathVariable("objectId") String objectId, @PathVariable("instanceId") String instanceId) {
        return objectRepository.findByObjectIdAndInstanceId(objectId, instanceId).orElseThrow(() -> new EntityNotFoundException(objectId + "/" + instanceId));
    }

    @GetMapping("/{objectId}")
    public List<Object> findByObjectId(@PathVariable("objectId") String objectId) {
        return objectRepository.findByObjectId(objectId);
    }

}
