package it.unirc.vo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import it.unirc.vo.dto.ResourceDto;
import it.unirc.vo.service.DataService;

import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping("/api/data")
public class DataController {

    private final DataService dataService;

    @Autowired
    public DataController(DataService dataService) {
        this.dataService = dataService;
    }

    @GetMapping("/{deviceId}/{objectId}/{instanceId}/{resourceId}/value")
    public List<ResourceDto> findValuesByResourceAndValue(@PathVariable("deviceId") String deviceId,
            @PathVariable("objectId") String objectId,
            @PathVariable("instanceId") String instanceId,
            @PathVariable("resourceId") String resourceId,
            @RequestParam("value") String value,
            @RequestParam(required = false) Integer operator) {
        return dataService.findValuesByResource(deviceId, objectId, instanceId, resourceId, value, operator);
    }

    @GetMapping("/{deviceId}/{objectId}/{instanceId}/{resourceId}/limit")
    public List<ResourceDto> findValuesByResourceAndCount(@PathVariable("deviceId") String deviceId,
            @PathVariable("objectId") String objectId,
            @PathVariable("instanceId") String instanceId,
            @PathVariable("resourceId") String resourceId,
            @RequestParam("limit") Integer limit) {
        return dataService.findValuesByResourceLimit(deviceId, objectId, instanceId, resourceId, limit);
    }

    @GetMapping("/{deviceId}/{objectId}/{instanceId}/{resourceId}/date")
    public List<ResourceDto> findValuesByResourceAndDate(@PathVariable("deviceId") String deviceId,
            @PathVariable("objectId") String objectId,
            @PathVariable("instanceId") String instanceId,
            @PathVariable("resourceId") String resourceId,
            @RequestParam("startDate") String startDate,
            @RequestParam("endDate") String endDate) throws ParseException {
        return dataService.findValuesByResourceAndDate(deviceId, objectId, instanceId, resourceId, startDate, endDate);
    }
}
