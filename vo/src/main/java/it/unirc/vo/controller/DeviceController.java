package it.unirc.vo.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.unirc.vo.model.Device;
import it.unirc.vo.repository.DeviceRepository;
import it.unirc.vo.service.DeviceService;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Log4j2
@RestController
@RequestMapping("/api/devices")
public class DeviceController {

    private final DeviceService deviceService;
    private final DeviceRepository deviceRepository;

    @Autowired
    public DeviceController(DeviceService deviceService, DeviceRepository deviceRepository) {
        this.deviceService = deviceService;
        this.deviceRepository = deviceRepository;
    }

    @GetMapping
    public List<Device> findAll() {
        return (List<Device>) deviceRepository.findAll();
    }

    @GetMapping("/{endpoint}")
    public Device find(@PathVariable("endpoint") String endpoint) {
        return deviceService.findByEndpoint(endpoint).orElseThrow(() -> new EntityNotFoundException(endpoint));
    }

}
