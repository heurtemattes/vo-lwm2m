﻿mvn clean install

docker build -t myorg/vo .

docker create -p 8080:8080 --name cvo_exposed -e "--server.port=8080" myorg/vo
docker create -p 8081:8081 --name d001_exposed -e "--server.port=8081" myorg/vo
docker create -p 8082:8082 --name d003_exposed -e "--server.port=8082" myorg/vo

docker cp C:\Nephere\GIT\VirtualObject\demo_utils\Application_registration\CVO\application-registration.yaml cvo_exposed:/application-registration.yaml
docker cp C:\Nephere\GIT\VirtualObject\demo_utils\Application_registration\D001\application-registration.yaml d001_exposed:/application-registration.yaml
docker cp C:\Nephere\GIT\VirtualObject\demo_utils\Application_registration\D003\application-registration.yaml d003_exposed:/application-registration.yaml