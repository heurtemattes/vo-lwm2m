import json
import paho.mqtt.client as mqtt
import time

from datetime import datetime



# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))





client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message



broker = "test.mosquitto.org"
D001="192.168.25.132"
D002="192.168.25.133"
D003="192.168.25.134"
CVO="192.168.25.135"

client.connect(broker, 1883, 60)

path_to_data = "C:\\Nephele\\Repo\\VirtualObject\\demo_utils\\datagen\\"

"""!!!!Check your path to files before execute this snippet !!!!"""
f1 = open(path_to_data + "data\\D003\\data_D003_3303_0_5700.json")
data3303 = json.load(f1)
print (str(len(data3303)))

f2 = open(path_to_data + "data\\D003\\data_D003_3320_0_5700.json")
data33200 = json.load(f2)
print (str(len(data33200)))

f3 = open(path_to_data + "data\\D003\\data_D003_3320_1_5700.json")
data33201 = json.load(f3)
print(str(len(data33201)))

f4 = open(path_to_data + "data\\D003\\data_D003_3323_0_5700.json")
data3323 = json.load(f4)
print(str(len(data3323)))

f5 = open(path_to_data + "data\\D003\\data_D003_3325_1_5700.json")
data3325 = json.load(f5)
print(str(len(data3325)))

f6 = open(path_to_data + "data\\D003\\data_D003_3326_0_5700.json")
data3326 = json.load(f6)
print(str(len(data3326)))

f7 = open(path_to_data + "data\\D003\\data_D003_26243_0_5700.json")
data26243 = json.load(f7)
print(str(len(data26243)))

"""TODO: Add inizialization of the data not in the json file"""

lenmax = max(len(data3303), len(data33200), len(data33201), len(data3323), len(data3325), len(data3326), len(data26243))

for i in (range(0,lenmax)):
    if i < len(data3303):
        topic = "tele/"+ str(data3303[i]['id_resource_vo'])
        text='{\"tmstp\":\"\",\"e\":[{\"v\":'+str(data3303 [i]['value'])+'}]}'
        dateTimeObj = datetime.now()
        client.publish(topic, payload=text, qos=0, retain=False)
        print(str(dateTimeObj) +"topic: " + data3303[i]['id_resource_vo']+ " payload: "+text)
        time.sleep(1)
    if i < len(data33200):
        topic = "tele/"+ str(data33200[i]['id_resource_vo'])
        text='{\"tmstp\":\"\",\"e\":[{\"v\":'+str(data33200 [i]['value'])+'}]}'
        client.publish(topic, payload=text, qos=0, retain=False)
        print("topic: " + data33200[i]['id_resource_vo']+ " payload: "+text)
        time.sleep(1)
    if i < len(data33201):
        topic = "tele/"+ str(data33201[i]['id_resource_vo'])
        text='{\"tmstp\":\"\",\"e\":[{\"v\":'+str(data33201 [i]['value'])+'}]}'
        client.publish(topic, payload=text, qos=0, retain=False)
        print("topic: " + data33201[i]['id_resource_vo']+ " payload: "+text)
        time.sleep(1)
    if i < len(data3323):
        topic = "tele/"+ str(data3323[i]['id_resource_vo'])
        text='{\"tmstp\":\"\",\"e\":[{\"v\":'+str(data3323 [i]['value'])+'}]}'
        client.publish(topic, payload=text, qos=0, retain=False)
        print("topic: " + data3323[i]['id_resource_vo']+ " payload: "+text)
        time.sleep(1)
    if i < len(data3325):
        topic = "tele/"+ str(data3325[i]['id_resource_vo'])
        text='{\"tmstp\":\"\",\"e\":[{\"v\":'+str(data3325 [i]['value'])+'}]}'
        client.publish(topic, payload=text, qos=0, retain=False)
        print("topic: " + data3325[i]['id_resource_vo']+ " payload: "+text)
        time.sleep(1)
    if i < len(data3326):
        topic = "tele/"+ str(data3326[i]['id_resource_vo']) 
        text='{\"tmstp\":\"\",\"e\":[{\"v\":'+str(data3326 [i]['value'])+'}]}'
        client.publish(topic, payload=text, qos=0, retain=False)
        print("topic: " + data3326[i]['id_resource_vo']+ " payload: "+text)
        time.sleep(1)
    if i < len(data26243):
        topic = "tele/"+ str(data26243[i]['id_resource_vo'])
        text='{\"tmstp\":\"\",\"e\":[{\"v\":'+str(data26243 [i]['value'])+'}]}'
        client.publish(topic, payload=text, qos=0, retain=False)
        print("topic: " + data26243[i]['id_resource_vo']+ " payload: "+text)
        time.sleep(1)
    

print ("Send ended.Disconnecting MQTT Client ...\n")
client.disconnect()
print("Closing file and exiting...")
# Closing file
f1.close()
f2.close()
f3.close()
f4.close()
f5.close()
f6.close()
f7.close()
print("Done!")
exit()
