#include "DHTesp.h" // Click here to get the library: http://librarymanager/All#DHTesp
#include <Ticker.h>

#include "EspMQTTClient.h"


#include <bits/stdc++.h>

#ifndef ESP32
#pragma message(THIS EXAMPLE IS FOR ESP32 ONLY!)
#error Select ESP32 board.
#endif

/**************************************************************/
/* Example how to read DHT sensors from an ESP32 using multi- */
/* tasking.                                                   */
/* This example depends on the Ticker library to wake up      */
/* the task every 20 seconds                                  */
/**************************************************************/

DHTesp dht;

void tempTask(void *pvParameters);
bool getTemperature();
void triggerGetTemp();

void setupClient ();



/** Task handle for the light value read task */
TaskHandle_t tempTaskHandle = NULL;
/** Ticker for temperature reading */
Ticker tempTicker;
/** Comfort profile */
ComfortState cf;
/** Flag if task should run */
bool tasksEnabled = false;
/** Pin number for DHT11 data pin */
int dhtPin = 33;

/** Task handle for the light value read task */
TaskHandle_t mqttTaskHandle = NULL;
/** Ticker for temperature reading */
Ticker mqttTicker;

/**Mqtt broker*/
const char* broker = "test.mosquitto.org"; 

EspMQTTClient client(
  "Gixs",
  "regalo12345",
  broker,  // MQTT Broker server ip
     // Can be omitted if not needed
    // Can be omitted if not needed
  "TestClient",     // Client name that uniquely identify your device
  1883              // The MQTT port, default to 1883. this line can be omitted
);


const String prefix = "tele";

const String deviceID = "D001";

//Object
const String topicTemperature = "3303/0";
const String topicHumidity = "3304/0";

//Values
const String lastValueResourceID = "5700";
const String maxRangeValueID = "5604";
const String minRangeValueID = "5603";
const String maxMeasuredValue = "5602";
const String minMeasuredValue = "5601";



float maxTemp;
float minTemp;
float maxHum;
float minHum;


TempAndHumidity newValues;


/**
 * initTemp
 * Setup DHT library
 * Setup task and timer for repeated measurement
 * @return bool
 *    true if task and timer are started
 *    false if task or timer couldn't be started
 */
bool initTemp() {
  
  //TODO ELIMINARE
  byte resultValue = 0;
  // Initialize temperature sensor
	dht.setup(dhtPin, DHTesp::DHT11);

  maxTemp = dht.getUpperBoundTemperature();
  minTemp = dht.getLowerBoundTemperature();

  maxHum = dht.getLowerBoundHumidity();
  minHum = dht.getUpperBoundHumidity();

	Serial.println("DHT initiated");

  // Start task to get temperature
	xTaskCreatePinnedToCore(
			tempTask,                       /* Function to implement the task */
			"tempTask ",                    /* Name of the task */
			4000,                           /* Stack size in words */
			NULL,                           /* Task input parameter */
			5,                              /* Priority of the task */
			&tempTaskHandle,                /* Task handle. */
			1);                             /* Core where the task should run */


  if (tempTaskHandle == NULL) {
    Serial.println("Failed to start task for temperature update");
    return false;
  } else {
    // Start update of environment data every 20 seconds
    tempTicker.attach(8, triggerGetTemp);
  }
  return true;
}

bool initMqtt () {

  // Optional functionalities of EspMQTTClient
  client.enableDebuggingMessages(); // Enable debugging messages sent to serial output
  client.enableHTTPWebUpdater(); // Enable the web updater. User and password default to values of MQTTUsername and MQTTPassword. These can be overridded with enableHTTPWebUpdater("user", "password").
  client.enableOTA(); // Enable OTA (Over The Air) updates. Password defaults to MQTTPassword. Port is the default OTA port. Can be overridden with enableOTA("password", port).
  client.enableLastWillMessage("TestClient/lastwill", "I am going offline");  // You can activate the retain flag by setting the third parameter to true

  Serial.println("Dentro di initMQTT");
  //TODO ELIMINARE
  byte resultValue = 0;


  Serial.println("MQTT initiated");

      // Start task MQTT
	xTaskCreatePinnedToCore(
			mqttTask,                       /* Function to implement the task */
			"mqttTask ",                    /* Name of the task */
			4000,                           /* Stack size in words */
			NULL,                           /* Task input parameter */
			4,                              /* Priority of the task */
			&mqttTaskHandle,                /* Task handle. */
			1);                             /* Core where the task should run */


  if (mqttTaskHandle == NULL) {
    Serial.println("Failed to start task for mqtt");
    return false;
  } else {
    // Start update of environment data every 20 seconds
    mqttTicker.attach(10, triggerMQTT);
  }
  return true;
}

/**
 * triggerGetTemp
 * Sets flag dhtUpdated to true for handling in loop()
 * called by Ticker getTempTimer
 */
void triggerGetTemp() {
  if (tempTaskHandle != NULL) {
	   xTaskResumeFromISR(tempTaskHandle);
  }
}

void triggerMQTT() {
  if (mqttTaskHandle != NULL) {
	   xTaskResumeFromISR(mqttTaskHandle);
  }
}

/**
 * Task to reads temperature from DHT11 sensor
 * @param pvParameters
 *    pointer to task parameters
 */
void tempTask(void *pvParameters) {
	Serial.println("tempTask loop started");
	while (1) // tempTask loop
  {
    if (tasksEnabled) {
      // Get temperature values
			getTemperature();
		}
    // Got sleep again
		vTaskSuspend(NULL);
	}
}

void mqttTask(void *pvParameters) {
	Serial.println("mqttTask loop started");
	while (1) // tempTask loop
  {
    if (tasksEnabled) {

      client.publish (prefix + "/" + deviceID + "/" + topicTemperature + "/" + lastValueResourceID, "{\"tmstp\":\"\",\"e\":[{\"v\":"+String (newValues.temperature)+"}]}");
      client.publish (prefix + "/" + deviceID + "/" + topicHumidity + "/" + lastValueResourceID, "{\"tmstp\":\"\",\"e\":[{\"v\":"+String (newValues.humidity)+"}]}" );

      
      
			client.loop();
		}
    // Got sleep again
		vTaskSuspend(NULL);
	}
}

/**
 * getTemperature
 * Reads temperature from DHT11 sensor
 * @return bool
 *    true if temperature could be aquired
 *    false if aquisition failed
*/
bool getTemperature() {
	// Reading temperature for humidity takes about 250 milliseconds!
	// Sensor readings may also be up to 2 seconds 'old' (it's a very slow sensor)
  newValues = dht.getTempAndHumidity();
 	// Check if any reads failed and exit early (to try again).
	if (dht.getStatus() != 0) {
		Serial.println("DHT11 error status: " + String(dht.getStatusString()));
		return false;
	}
  

  

	float heatIndex = dht.computeHeatIndex(newValues.temperature, newValues.humidity);
  float dewPoint = dht.computeDewPoint(newValues.temperature, newValues.humidity);
  float cr = dht.getComfortRatio(cf, newValues.temperature, newValues.humidity);

  String comfortStatus;
  switch(cf) {
    case Comfort_OK:
      comfortStatus = "Comfort_OK";
      break;
    case Comfort_TooHot:
      comfortStatus = "Comfort_TooHot";
      break;
    case Comfort_TooCold:
      comfortStatus = "Comfort_TooCold";
      break;
    case Comfort_TooDry:
      comfortStatus = "Comfort_TooDry";
      break;
    case Comfort_TooHumid:
      comfortStatus = "Comfort_TooHumid";
      break;
    case Comfort_HotAndHumid:
      comfortStatus = "Comfort_HotAndHumid";
      break;
    case Comfort_HotAndDry:
      comfortStatus = "Comfort_HotAndDry";
      break;
    case Comfort_ColdAndHumid:
      comfortStatus = "Comfort_ColdAndHumid";
      break;
    case Comfort_ColdAndDry:
      comfortStatus = "Comfort_ColdAndDry";
      break;
    default:
      comfortStatus = "Unknown:";
      break;
  };

  Serial.println(" T:" + String(newValues.temperature) + " H:" + String(newValues.humidity) + " I:" + String(heatIndex) + " D:" + String(dewPoint) + " " + comfortStatus);
	return true;
}

void setup()
{
  Serial.begin(115200);
  Serial.println();
  Serial.println("DHT ESP32 example with tasks");
  initTemp();

  Serial.println("Prima di initMQTT");
  initMqtt();
  // Signal end of setup() to tasks
  tasksEnabled = true;
}

boolean firstTime = true;

void onConnectionEstablished()
{
  if (firstTime) {
    float upperBoundTemperature = dht.getUpperBoundTemperature();
    float lowerBoundTemperature = dht.getLowerBoundTemperature();
    float upperBoundHumidity = dht.getUpperBoundHumidity();
    float lowerBoundHumidity = dht.getLowerBoundHumidity();

    client.publish (prefix + "/" + deviceID + "/" + topicTemperature + "/" + maxRangeValueID, "{\"tmstp\":\"\",\"e\":[{\"v\":"+String(upperBoundTemperature)+"}]}" );
    client.publish (prefix + "/" + deviceID + "/" + topicTemperature + "/" + minRangeValueID, "{\"tmstp\":\"\",\"e\":[{\"v\":"+String(lowerBoundTemperature)+"}]}" );
    client.publish (prefix + "/" + deviceID + "/" + topicHumidity + "/" + maxRangeValueID, "{\"tmstp\":\"\",\"e\":[{\"v\":"+String(upperBoundHumidity)+"}]}" );
    client.publish (prefix + "/" + deviceID + "/" + topicHumidity + "/" + minRangeValueID, "{\"tmstp\":\"\",\"e\":[{\"v\":"+String(lowerBoundHumidity)+"}]}" );
    firstTime = false;
  }

  Serial.println("MQTT CONNECTION with broker "+ String(broker) +"  ESTABLISHED.");
}

void loop() {
  if (!tasksEnabled) {
    // Wait 2 seconds to let system settle down
    delay(2000);
    // Enable task that will read values from the DHT sensor
    tasksEnabled = true;
    if (tempTaskHandle != NULL) {
			vTaskResume(tempTaskHandle);
		}
    if (mqttTaskHandle != NULL) {
      vTaskResume(mqttTaskHandle);
    }
  }
  yield();
}
