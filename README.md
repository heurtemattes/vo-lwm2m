# Virtual Object [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

The VO is a service that represents the virtual counterpart of a physical device, to which it is interconnected, by enhancing its capability. 
It is composed of:
- Application Interfaces (Northbound)
  Dedicated to communication with entities like data consumers, applications or users. Such interfaces may use HTTP and include graphical interfaces.
- Backend Logics
  They include all the logics related to operational behaviors, coded executables and even the services that the Object/Device can perform.
- IoT Interfaces (Southbound)
  They use IoT protocols in order to communicate with the corresponding physical device.



# How To - Quick Tutorials
Series of quick tutorials on Virtual Object startup.

## Manual start up
The first tutorial is for starting VO manually, without using docker, and using maven. 

### Prerequisites 

- Maven 
- Java
- Docker
- InfluxDB (info must be writed on the configuration file, see step 3)

If wanna use the configuration used in the demo for influx DB:
Open a terminal:
 ```bash
sudo docker pull influxdb
```

```bash
sudo docker run -d -p 8086:8086 --name influxdb2 -v $PWD/data:/var/lib/influxdb2 -v
$PWD/config:/etc/influxdb2 -e DOCKER_INFLUXDB_INIT_MODE=setup -e
DOCKER_INFLUXDB_INIT_USERNAME=root -e
DOCKER_INFLUXDB_INIT_PASSWORD=rootroot -e DOCKER_INFLUXDB_INIT_ORG=UniRC -e
DOCKER_INFLUXDB_INIT_BUCKET=vo_data -e
DOCKER_INFLUXDB_INIT_ADMIN_TOKEN=token influxdb:2.6
```
  
### Step

1. Clone this repository.
2. Open your terminal, go in the vo-lwm2m/vo folder and build the project using maven:  

```bash
mvn clean install
```

That will create a *.jar* file in the *vo/target* folder.

3. Before starting the VO we need to include in the same directory as the executable of the VO (.jar) a config folder with an application-registration.yaml file inside. This file serves as VO configurator. You can find some .yaml file created as a test in the *demo_utils/application_registration*. If you need to create a standard VO use the *D001* or *D003*. If you need a composite VO that incorporate *D001* and *D003* use the CVO one. You can create your own conf file, specifing the info you need. As an example use *D001*, once you are in the *vo-lwm2m/vo*:
```bash
cp ../demo_utils/Application_registration/D001/application-registration.yaml target/
```
4. Once the step 3 is correctly done, influxDB on the docker container is started, go in the target folder and run the jar file:
```bash
cd target
```
```bash
java -jar vo-1.0-SNAPSHOT.jar
```
# Test

### MQTT Messages Test

Resource and Object

MQTT messages used to test VO southbound interface:
***Before sending messagge, check the mqtt broker url is matching with your mqtt broker. In this example the MQtt broker address is "test.mosquitto.org".

Public a resource value 3303/0/5700 (using mosquitto cmdline):
```
.\mosquitto_pub.exe -h test.mosquitto.org -t tele/D001/3303/0/5700 -m "{\"tmstp\":\"\",\"e\":[{\"v\":28}]}" -d
```
Public an object instance 3303/0 ( using mosquitto cmdline):
```
.\mosquitto_pub.exe -h test.mosquitto.org -t tele/D001/3303/0 -m "{\"tmstp\":\"\",\"e\":[{\"n\":5700,\"v\":30},{\"n\":5601,\"v\":30},{\"n\":5603,\"v\":30}]}" -d
```

Read the resource value from HTTP READ:
curl --location --request GET 'localhost:8080/api/clients/D001/3303/0/5700'

### Composite VO
To simulate sending a resource value from VO to CVO:
HTTP POST with JSON payload:
```
> {"event":"NOTIFICATION","data":{"ep":"deviceID","res":"/3303/0/5700", "val":{"id":5700, "value":22}}}
```


# Virtual Object Environment
  The environment surrounding a Virtual Object (VO) is multifaceted, encompassing various components that facilitate its operation and interaction within the computing continuum. This section delves into these primary components, shedding light on their roles and functionalities.

  ![voarchitecture](/images/vo_architecture.png)

### Southbound component
The Southbound component serves as the bridge between the VO and the physical device. Through this, the VO communicates using transfer protocols such as HTTP, COAP, and MQTT. This component ensures that the VO can seamlessly interact with the physical device, gathering data and sending commands as necessary.

### Northbound Component
On the opposite side, the Northbound component represents the interface through which the VO communicates with external applications. This component is crucial for the VO to relay the data it gathers and to receive instructions or queries from applications that utilize its services.

### Storage
The Storage component is where the VO retains data collected from the physical device. This is not just a passive data repository; it's an active component that ensures data integrity, availability, and consistency. Depending on the requirements, this storage can be local or cloud-based, providing flexibility in data management and retrieval.

### Orchestration
Orchestration is the mechanism that allows the VO to be generated, integrated, and managed within the system. It ensures that the VO operates in harmony with other components, facilitating its creation, updates, and potential decommissioning. This component plays a pivotal role in ensuring the VO's seamless operation across the broader computing continuum.

## Protocol Bindings
Among the various IoT protocols examined for the implementation of these interfaces, it was decided to use the Message Queue Telemetry Transport (MQTT) protocol in the first instance, suitably adapted to the project needs. The integration of the IoT CoAP protocol, based on the Eclipse Leshan project, is in the final integration and testing phase, after which the related documentation will be released.

For the definition of the MQTT topics used for the subscription and publication of data, it was decided to use the OMA LwM2M semantics and, in particular, the path-URI of the resources as defined in the OMA-LwM2M protocol. The payload is always defined according to the OMA-LWM2M model using the JSON format. 
  
  ### MQTT
  The addressing of resources will take place through the definition of dedicated topics. In this implementation we distinguish:
  - **topic:** 
  - **fulltopic:** which requires a prefix (eg cmnd) to precede the actual topic.

Specifically, the topics are used following the standard modeling of the OMA-LwM2M protocol, which identifies a unique URI for each resource. The unique identifiers of the resources are defined through the use of a semantic standard reported in the OMA Registry1. By way of example, the OMA-LwM2M object with ID 6 describes the position of a device, therefore, the topic of the Longitude resource, identified with id 1, of instance 0 of the Location object (6) of the Bus device (B001) will be formed as follows: B001/6/0/1.

The FullTopic prefixes are used to avoid the possibility of  creating any loops between MQTT topics and to be able to distinguish messages directly from the type of prefix used. Three distinct prefixes will be used in this implementation:
- **cmnd:** for issuing commands and querying resource status;
- **stat:** reports the configuration status or message;
- **tele:** reports telemetry information at specified intervals or by events.

Using the topics described above it is therefore possible to implement the interfaces that enable communications between
physical devices and the VOs. The devices must make interfaces available on specific topics to receive the requests sent
by the VOs and, consequently, enable publication on the topics on which it will be possible to listen to the data published
by them.

The primitives are as follows:
- **READ:** requires the reading of one or more resources;
- **WRITE:** modify one or more resources;
- **EXECUTE:**  trigger an execution;
- **OBSERVE:** activate the observation of one or more resources;
- **DELETE OBSERVE:** delete a previous observation of one or more resources.

#### READ
The first table concerns the message that the VO will have to send to request the sending of the data to the physical
counterpart (the resource, the instance or the object), therefore the request.

**Resource**
|<span>||
|---|---|
|Topic|``cmnd/deviceID/objId/instId/resId``|
|Payload| not required |

**Instance**
|<span>||
|---|---|
|Topic|``cmnd/deviceID/objId/instId``|
|Payload| not required |

**Object**
|<span>||
|---|---|
|Topic|``cmnd/deviceID/objId``|
|Payload| not required |

## Database schema (sqlite & influx db)
In this section is described the database entitites used to manage the information inside the Virtual Object. 
The schema is divided in two different part: the most static one (managed by SQL lite) and the most variable (and accessed) part, managed by influx DB.

![dbschema](/images/dbschema.png)

### SQL Lite
In the SQL Lite, are implemented the table that are rarely changed.

Those tables are:
1. **Device:** this table contains the descriptive attributes of the device such as VO name (endpointname), lwm2m version, registrationId, etc.;
2. **Object:** contains the LwM2M objects contained by the VO and their attributes as defined by OMA-LwM2M;
3. **Observable:** they are the entities (instance or resource) placed under observation by one or more observers;
4. **Observer:** contains the list of all external applications that have requested the observation of a resource;
5. **Resource:** stores the list of resources made available by enabled OMA-LwM2M objects;

### InfluxDB 
In influxDB, are implemented the table that changes more frequently. 
1. **Measurement table**;
2. **Event table**;

### Table in depth
Shown below is a in deep description for every column of the tables.

#### DEVICE

This table store the physical device that comunicate with the VO in the southbound API.
The Device table has 15 columns:
1. **id:** specifies the primary key of the entity and it's automatically generated;
2. **additional_registration_attributes:** (optional) for future developtment;
3. **address:** IP address and port where
4. **binding_mode:** indicates the interface type used for the device comunication;
5. **created_at:** timestamp when the device is created;
6. **endpoint:** the unique name of the device;
7. **lifetime:** lifetime keep-alive (not used at the moment)
8. **lwm2m_version:** version of the Lightweight M2M protocol used;
9. **registration_endpoint_address:** not used at the moment
10. **registration_id:** unique identifier of the registration;
11. **resource_type:** type of semantics used (eg. oma.lwm2m)
12. **root_path:** path for the exposed resources;
13. **secure:** [TRUE/FALSE] enable/disable the security protocol;
14. **sms_number:** SIM number of the device;
15. **updated_at:** timestamp of the last update.

#### OBJECT

Register in the database the object used in the VO, following the OMA LightweightM2M [registry](https://technical.openmobilealliance.org/OMNA/LwM2M/LwM2MRegistry.html).

The Object class is an extension of Observable, so include the *id* and *name* parameters.

The Object table has 9 columns:
1. **id:** check observable;
2. **name:** check observable;
3. **created_at:** timestamp when the object is added to the VO system;
4. **description:** small description of the object;
5. **instance_id:** different object can be instantiate multiple times and all of those instances are stored in the object table. This *varchar* indicates the object instance;
6. **mandatory:** Mandatory (must be supported by all LwM2M Client implementations) or Optional (may not be supported);
7. **object_id:** the object id following the OMA Lwm2m registry;
8. **updated_at:** last timestamp in which the VO received an update from the device;
9. **device_id:** the device id where the object is built in;

#### RESOURCE
The resource table is the list of the OMA resource bounded to the object instance.

The resource table has 13 columns:
1. **id:** uniqueID of the resource;
2. **name:** name of the rapresented resource (eg. Sensor Value, Min Measured Value)
3. **created_at:** timestamp when the resource is created;
4. **description:** brief description of the resource;
5. **instance_type:** check standard Oma-lwm2m;
6. **mandatory:** check standard Oma-lwm2m;
7. **operations:** shows the type of the operations that can be applied to the resource: NONE, R(read), W(write), RW, E(executable), RE, WE, RWE;
8. **range_enumeration:**
9. **resource_id:** OMA registry uniqueID of the resource;
10. **type:** indicates the type of resource: STRING, INTEGER, FLOAT, BOOLEAN, OPAQUE, TIME, OBJLNK;
11. **units:** check standard Oma-lwm2m;
12. **updated_at:** timestamp of the last update;
13. **object_id:** Object to which the resource reffers;


#### OBSERVABLE
The Northbound interface of the VO gives the possibility through an API to get notified for each update that an *object* or a *resource* has. This capability to be "observed" gives the object the attributes "observable". So when an application send an *observe request* for a given *object* or *resource*, the object or the *resource* is stored in the table **observable**.

This table stores the object created by the class that extends Observable.

1. **id:** specifies the primary key of the entity and it's automatically generated;
2. **name:** specifies the name of the observable;

#### OBSERVER
The observers are the object (application, other VOs, cVOs) that observe. In other words, the observer table is the list of the consumers that has requested the observation on a resoruce or an object instance. The observer will be notifyed for each resource or object instance update.

The observer table has 7 columns:
1. **id:** uniqueID of the observer;
2. **address:** address where the observer response has to be sended. This address is sent in the payload of the *observe request*. In the case of null payload, this address is equals to the request sender;
3. **created_at:** timestap when the observer is instanciated;
4. **name:** not used at the moment;
5. **one_shot:** MQTT is not a Req/resp protocol. So that, this flag is used to enable an Async Request/Response behaviour into northbound interface, HTTP, and the external application (Request) making one shot observable the required resource until the southbound, MQTT, get the new data.
6. **updated_at:** timestamp of the last update;
7. **observable_id:** uniqueID of the object for which the observer want to receive notifications;

#### EVENT (InfluxDB)
The Event table is implemented like an influxDB table and has 2 column:
1. **timestamp:** timestamp when the Virtual Object is notified by the device;
2. **JSON:** given the fact the VO-LwM2M communicates in the Southbound using JSON, each event arrives in JSON format and it is saved in row in this table.


#### Measurement Table (InfluxDB)
The Measurement table is used to store the values notified by the device, for each resource. It is a InfluxDB table and it has 3 columns:
1. **timestamp:** denotes the timestamp of the data entry.
2. **value:** holds the actual measurement value. For convenience, this attribute is further divided into various variables based on the value type,
such as ”valueString”, ”valueInteger”, and so on. However, only one
of these variables will be instantiated and populated with the measurement’s value.
3. **resource_id:** represents the OMA resource receiving updates;
4. **type:** specifies the kind of value being received;