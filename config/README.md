# Configuration file
This folder is where the configuration file ``application_registration.yaml`` goes.

The configuration file is built like the following entry.
Check the example one in the folder ``demo_utils\application_registration``

``` yaml

vo:
  device:
    endpoint: #physical device unique name
    registrationId: 
    address: #ip where VO is present on the orchestretion
    Version: #VO Version
    lifetime: #lifetime keep alive (not used atm)
    bindingMode: #interface type for southbound (M = MQTT, U = COAP, H = HTTP)
    rootPath: #path for resources 
    resourceType: #resource semantic type 
    secure: #secure mode (not used)
    additionalRegistrationAttributes:
  objectLinks: #objectinstance link 
    - 
    
  url: 
  cleanSession: true #opzione MQTT
  username: #MQTT usr
  password: #MQTT pssw
  mqttQos:  #MQTT QoS

  #INFLUX DB PROPERTIES
influx:
  url: # URL to connect to InfluxDB.
  username: # Username to use in the basic auth.
  password: # Password to use in the basic auth.
  token: # Token to use for the authorization.
  org: # Default destination organization for writes and queries.
  bucket: # Default destination bucket for writes.
  logLevel: # The log level for logging the HTTP request and HTTP response. (Default: NONE)
  readTimeout: # Read timeout for OkHttpClient. (Default: 10s)
  writeTimeout: # Write timeout for OkHttpClient. (Default: 10s)
  connectTimeout: # Connection timeout for OkHttpClient. (Default: 10s)

management:
  endpoint:
    metrics:
      enabled: false


server:
  port: #port used

```